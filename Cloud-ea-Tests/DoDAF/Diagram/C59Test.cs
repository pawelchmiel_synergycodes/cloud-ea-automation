﻿using Automated_Framework_2._0.Cloud_EA.DoDAF.Diagram;
using Automated_Framework_2._0.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Cloud_ea_Tests.DoDAF.Diagram
{
    [TestClass]
    public class C59Test : BaseTest
    {
        [TestMethod]
        public void TestC59_CV1()
        {
            // Initialzing Web browser and Applitools Eyes
            Driver.Initialize("Cloud-EA", "C59 - CV1");

            CreateTestWorkspace();
            CV1Page.TestC59(true);

            // Web browser driver needs to be closed every time test ends
            Driver.CleanUp();
        }
    }
}
