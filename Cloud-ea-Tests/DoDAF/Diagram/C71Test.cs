﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Automated_Framework_2._0.Utility;
using Automated_Framework_2._0.Cloud_EA.DoDAF.Diagram;

namespace Cloud_ea_Tests.DoDAF.Diagram
{
    [TestClass]
    public class C71Test : BaseTest
    {
        [TestMethod]
        public void TestC71_OV4()
        {
            // Initialzing Web browser and Applitools Eyes
            Driver.Initialize("Cloud-EA", "C71 - OV4");

            CreateTestWorkspace();
            OV4Page.TestC71(true);

            // Web browser driver needs to be closed every time test ends
            Driver.CleanUp();
        }
    }
}
