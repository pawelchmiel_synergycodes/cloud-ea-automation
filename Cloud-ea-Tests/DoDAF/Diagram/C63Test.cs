﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Automated_Framework_2._0.Utility;
using Automated_Framework_2._0.Cloud_EA.DoDAF.Diagram;

namespace Cloud_ea_Tests.DoDAF.Diagram
{
    [TestClass]
    public class C63Test : BaseTest
    {
        [TestMethod]
        public void TestC63_CV4()
        {
            // Initialzing Web browser and Applitools Eyes
            Driver.Initialize("Cloud-EA", "C63 - CV4");

            CreateTestWorkspace();
            CV4Page.TestC63(true);

            // Web browser driver needs to be closed every time test ends
            Driver.CleanUp();
        }
    }
}
