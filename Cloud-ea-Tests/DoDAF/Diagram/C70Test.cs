﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Automated_Framework_2._0.Utility;
using Automated_Framework_2._0.Cloud_EA.DoDAF.Diagram;

namespace Cloud_ea_Tests.DoDAF.Diagram
{
    [TestClass]
    public class C70Test : BaseTest
    {
        [TestMethod]
        public void TestC70_OV5a()
        {
            // Initialzing Web browser and Applitools Eyes
            Driver.Initialize("Cloud-EA", "C70 - OV5a");

            CreateTestWorkspace();
            OV5aPage.TestC70(true);

            // Web browser driver needs to be closed every time test ends
            Driver.CleanUp();
        }
    }
}
