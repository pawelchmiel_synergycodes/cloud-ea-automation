﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Automated_Framework_2._0.Utility;
using Automated_Framework_2._0.Cloud_EA.DoDAF.Diagram;

namespace Cloud_ea_Tests.DoDAF.Diagram
{
    [TestClass]
    public class C93Test : BaseTest
    {
        [TestMethod]
        public void TestC93_DIV2()
        {
            // Initialzing Web browser and Applitools Eyes
            Driver.Initialize("Cloud-EA", "C93 - DIV2");

            CreateTestWorkspace();
            DIV2Page.TestC93(true);

            // Web browser driver needs to be closed every time test ends
            Driver.CleanUp();
        }
    }
}
