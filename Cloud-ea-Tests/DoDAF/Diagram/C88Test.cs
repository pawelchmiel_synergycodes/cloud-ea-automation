﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Automated_Framework_2._0.Utility;
using Automated_Framework_2._0.Cloud_EA.DoDAF.Diagram;

namespace Cloud_ea_Tests.DoDAF.Diagram
{
    [TestClass]
    public class C88Test : BaseTest
    {
        [TestMethod]
        public void TestC88_DIV1()
        {
            // Initialzing Web browser and Applitools Eyes
            Driver.Initialize("Cloud-EA", "C88 - DIV1");

            CreateTestWorkspace();
            DIV1Page.TestC88(true);

            // Web browser driver needs to be closed every time test ends
            Driver.CleanUp();
        }
    }
}
