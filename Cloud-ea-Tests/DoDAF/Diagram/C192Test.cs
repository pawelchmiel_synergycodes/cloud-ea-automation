﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Automated_Framework_2._0.Utility;
using Automated_Framework_2._0.Cloud_EA.DoDAF.Diagram;

namespace Cloud_ea_Tests.DoDAF.Diagram
{
    [TestClass]
    public class C192Test : BaseTest
    {
        [TestMethod]
        public void TestC192_SvcV4a()
        {
            // Initialzing Web browser and Applitools Eyes
            Driver.Initialize("Cloud-EA", "C192 - SvcV4a");

            CreateTestWorkspace();
            SvcV4aPage.TestC192(true);

            // Web browser driver needs to be closed every time test ends
            Driver.CleanUp();
        }
    }
}
