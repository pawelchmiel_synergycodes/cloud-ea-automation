﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Automated_Framework_2._0.Utility;
using Automated_Framework_2._0.Cloud_EA.DoDAF.Diagram;

namespace Cloud_ea_Tests.DoDAF.Diagram
{
    [TestClass]
    public class C96Test : BaseTest
    {
        [TestMethod]
        public void TestC96_OV2()
        {
            // Initialzing Web browser and Applitools Eyes
            Driver.Initialize("Cloud-EA", "C96 - OV2");

            CreateTestWorkspace();
            OV2Page.TestC96(true);

            // Web browser driver needs to be closed every time test ends
            Driver.CleanUp();
        }
    }
}
