﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Automated_Framework_2._0.Utility;
using Automated_Framework_2._0.Cloud_EA.DoDAF.Diagram;

namespace Cloud_ea_Tests.DoDAF.Diagram
{
    [TestClass]
    public class C132Test : BaseTest
    {
        [TestMethod]
        public void TestC132_PV1()
        {
            // Initialzing Web browser and Applitools Eyes
            Driver.Initialize("Cloud-EA", "C132 - PV1");

            CreateTestWorkspace();
            PV1Page.TestC132(true);

            // Web browser driver needs to be closed every time test ends
            Driver.CleanUp();
        }
    }
}
