﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Automated_Framework_2._0.Utility;
using Automated_Framework_2._0.Cloud_EA.DoDAF.Diagram;

namespace Cloud_ea_Tests.DoDAF.Diagram
{
    [TestClass]
    public class C157Test : BaseTest
    {
        [TestMethod]
        public void TestC157_OV5a()
        {
            // Initialzing Web browser and Applitools Eyes
            Driver.Initialize("Cloud-EA", "C157 - OV5a");

            CreateTestWorkspace();
            OV5aPage.TestC157(true);

            // Web browser driver needs to be closed every time test ends
            Driver.CleanUp();
        }
    }
}
