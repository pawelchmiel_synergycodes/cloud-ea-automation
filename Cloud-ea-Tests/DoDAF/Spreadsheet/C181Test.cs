﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Automated_Framework_2._0.Utility;
using Automated_Framework_2._0.Cloud_EA.DoDAF.Diagram;

namespace Cloud_ea_Tests.DoDAF.Diagram
{
    [TestClass]
    public class C181Test : BaseTest
    {
        [TestMethod]
        public void TestC181_SvcV4b()
        {
            // Initialzing Web browser and Applitools Eyes
            Driver.Initialize("Cloud-EA", "C181 - SvcV4b");

            CreateTestWorkspace();
            SvcV4bPage.TestC181(true);

            // Web browser driver needs to be closed every time test ends
            Driver.CleanUp();
        }
    }
}
