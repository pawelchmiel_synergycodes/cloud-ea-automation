﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Automated_Framework_2._0.Utility;
using Automated_Framework_2._0.Cloud_EA.DoDAF.Diagram;

namespace Cloud_ea_Tests.DoDAF.Diagram
{
    [TestClass]
    public class C186Test : BaseTest
    {
        [TestMethod]
        public void TestC186_SV4b()
        {
            // Initialzing Web browser and Applitools Eyes
            Driver.Initialize("Cloud-EA", "C186 - SV4b");

            CreateTestWorkspace();
            SV4bPage.TestC186(true);

            // Web browser driver needs to be closed every time test ends
            Driver.CleanUp();
        }
    }
}
