﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Automated_Framework_2._0.Utility;
using Automated_Framework_2._0.Cloud_EA.DoDAF.Diagram;

namespace Cloud_ea_Tests.DoDAF.Diagram
{
    [TestClass]
    public class C176Test : BaseTest
    {
        [TestMethod]
        public void TestC176_OV5b()
        {
            // Initialzing Web browser and Applitools Eyes
            Driver.Initialize("Cloud-EA", "C176 - OV5b");

            CreateTestWorkspace();
            OV5bPage.TestC176(true);

            // Web browser driver needs to be closed every time test ends
            Driver.CleanUp();
        }
    }
}
