﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Automated_Framework_2._0.Utility;
using Automated_Framework_2._0.Cloud_EA.DoDAF.Diagram;

namespace Cloud_ea_Tests.DoDAF.Diagram
{
    [TestClass]
    public class C184Test : BaseTest
    {
        [TestMethod]
        public void TestC184_SV2()
        {
            // Initialzing Web browser and Applitools Eyes
            Driver.Initialize("Cloud-EA", "C184 - SV2");

            CreateTestWorkspace();
            SV2Page.TestC184(true);

            // Web browser driver needs to be closed every time test ends
            Driver.CleanUp();
        }
    }
}
