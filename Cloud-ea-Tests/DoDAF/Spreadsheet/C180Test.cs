﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Automated_Framework_2._0.Utility;
using Automated_Framework_2._0.Cloud_EA.DoDAF.Diagram;

namespace Cloud_ea_Tests.DoDAF.Diagram
{
    [TestClass]
    public class C180Test : BaseTest
    {
        [TestMethod]
        public void TestC180_SvcV4a()
        {
            // Initialzing Web browser and Applitools Eyes
            Driver.Initialize("Cloud-EA", "C180 - SvcV4a");

            CreateTestWorkspace();
            SvcV4aPage.TestC180(true);

            // Web browser driver needs to be closed every time test ends
            Driver.CleanUp();
        }
    }
}
