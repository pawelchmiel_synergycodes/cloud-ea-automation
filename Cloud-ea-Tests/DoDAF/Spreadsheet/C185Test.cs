﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Automated_Framework_2._0.Utility;
using Automated_Framework_2._0.Cloud_EA.DoDAF.Diagram;

namespace Cloud_ea_Tests.DoDAF.Diagram
{
    [TestClass]
    public class C185Test : BaseTest
    {
        [TestMethod]
        public void TestC185_SV4a()
        {
            // Initialzing Web browser and Applitools Eyes
            Driver.Initialize("Cloud-EA", "C185 - SV4a");

            CreateTestWorkspace();
            SV4aPage.TestC185(true);

            // Web browser driver needs to be closed every time test ends
            Driver.CleanUp();
        }
    }
}
