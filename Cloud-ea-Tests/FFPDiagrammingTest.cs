﻿using System.Configuration;
using Automated_Framework_2._0;
using Automated_Framework_2._0.Cloud_EA.DoDAF;
using Automated_Framework_2._0.Pages;
using Automated_Framework_2._0.Utility;
using Automated_Framework_2._0.Utility.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Cloud_ea_Tests
{
    [TestClass]
    public class FFPDiagrammingTest
    {
        private readonly App _app = new App(AppsNames.DodafFfpDiagramming);
        private readonly string _userName = ConfigurationManager.AppSettings["UserName"];
        private readonly string _userPassword = ConfigurationManager.AppSettings["UserPassword"];

        [TestMethod]
        public void _FPPDiagrammingTest1()
        {
            // Initialzing Web browser and Applitools Eyes
            Driver.Initialize("Cloud-EA", "FFP Diagramming");

            LoginPage.NavigateToLoginPage();
            LoginPage.Login(_userName, _userPassword);

            DashboardPage.NavigateToApp(_app);

            FFPDiagramming.DropTwoObjectOntoCanvaAndMakeRelationship();

            // Web browser driver needs to be closed every time test ends
            Driver.CleanUp();
        }     
    }
}
