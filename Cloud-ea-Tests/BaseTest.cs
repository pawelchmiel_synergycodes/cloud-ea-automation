﻿using Automated_Framework_2._0;
using Automated_Framework_2._0.Cloud_EA.DoDAF.RepositoryManagement;
using Automated_Framework_2._0.Pages;
using Automated_Framework_2._0.Utility;
using Automated_Framework_2._0.Utility.Enums;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cloud_ea_Tests
{
    public class BaseTest
    {
        private readonly string _userName = ConfigurationManager.AppSettings["UserName"];
        private readonly string _userPassword = ConfigurationManager.AppSettings["UserPassword"];

        protected void CreateTestWorkspace()
        {
            LoginPage.NavigateToLoginPage();
            LoginPage.Login(_userName, _userPassword);

            DashboardPage.NavigateToApp(new App(AppsNames.DodafRepositoryManagementApp));
            WorkspaceMgmnt.CreateWorkspace("__AutoTest");
        }

        protected void DeleteTestWorkspace()
        {
            LoginPage.NavigateToLoginPage();
            LoginPage.Login(_userName, _userPassword);

            DashboardPage.NavigateToApp(new App(AppsNames.DodafRepositoryManagementApp));
            WorkspaceMgmnt.DeleteWorkspace("__AutoTest");
        }
    }
}
