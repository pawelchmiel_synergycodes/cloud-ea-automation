﻿namespace Automated_Framework_2._0.Utility
{
    public class EmailMessage
    {
        public long id { get; set; }
        public string html_body { get; set; }
        public string filename { get; set; }
    }
}
