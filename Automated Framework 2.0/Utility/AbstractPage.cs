﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using MouseKeyboardLibrary;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.Extensions;
using OpenQA.Selenium.Support.UI;
using Keys = System.Windows.Forms.Keys;

namespace Automated_Framework_2._0.Utility
{
    public class AbstractPage
    {
        private static WebDriverWait _wait;
        private static readonly int _timeToWait = 30;
        public static IWebDriver GetDriver()
        {
            return Driver.Instance;
        }

        public static void Open(string url)
        {
            GetDriver().Navigate().GoToUrl(url);
        }

        public static IWebElement FindElement(By locator)
        {
            Actions action = new Actions(GetDriver());
            action.MoveToElement(GetDriver().FindElement(locator)); // scrolls to element into view
            return GetDriver().FindElement(locator);
        }

        public static ReadOnlyCollection<IWebElement> FindElements(By locator)
        {
            return GetDriver().FindElements(locator);
        }

        public static void Click(By locator)
        {
            _wait = new WebDriverWait(GetDriver(), TimeSpan.FromSeconds(_timeToWait));
            _wait.Until(ExpectedConditions.ElementToBeClickable(locator));
            FindElement(locator).Click();
        }

        public static void TypeInto(By locator, string text)
        {
            FindElement(locator).Clear();
            FindElement(locator).SendKeys(text);
        }

        public static void WaitForElementToBeVisible(By locator)
        {
            _wait = new WebDriverWait(GetDriver(), TimeSpan.FromSeconds(_timeToWait));
            _wait.Until(ExpectedConditions.ElementIsVisible(locator));
        }

        public static void WaitForElementToBePresentInDom(By locator)
        {
            _wait = new WebDriverWait(GetDriver(), TimeSpan.FromSeconds(_timeToWait));
            _wait.Until(ExpectedConditions.ElementExists(locator));
        }

        public static void WaitForElementToBeInsivible(By locator)
        {
            _wait = new WebDriverWait(GetDriver(), TimeSpan.FromSeconds(_timeToWait));
            _wait.Until(ExpectedConditions.InvisibilityOfElementLocated(locator));
        }

        /// <summary>
        /// Gerts element names by HTML script executed in browser.
        /// </summary>
        /// <param name="locator"></param>
        /// <returns></returns>
        public static List<string> GetTextFromElements(By locator)
        {
            WaitForElementToBePresentInDom(locator);
            var elements = FindElements(locator);
            return elements.Select(element => GetDriver().ExecuteJavaScript<string>("return arguments[0].innerHTML", element)).ToList();
        }

        public static void SelectTextFromDropdown(By dropdownLocator, string text)
        {
            new SelectElement(FindElement(dropdownLocator)).SelectByText(text);
        }

        public static string GetPageTitle()
        {
            return GetDriver().Title;
        }

        /// <summary>
        /// Returns number in array of elements based on element name.
        /// Using Script.
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="itemName"></param>
        /// <returns></returns>
        public static int GetElementNumberInElementsArray(By locator, string itemName)
        {
            var elementsNames = GetTextFromElements(locator);
            for (int i = 0; i < elementsNames.Count; i++)
            {
                if (elementsNames[i] != itemName) continue;
                return i + 1;
            }
            return 0;
        }

        /// <summary>
        /// Returns number in array of elements based on element name. 
        /// Using text() method. Index starts at 1.
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="itemName"></param>
        /// <returns></returns>
        public static int GetElementNumberInElementsArray2(By locator, string itemName)
        {
            var elementsNames = GetTextFromElements2(locator);
            for (int i = 0; i < elementsNames.Count; i++)
            {
                if (elementsNames[i] != itemName) continue;
                return i + 1;
            }
            return 0;
        }

        /// <summary>
        /// Gets elements names using text() method from Selenium.
        /// </summary>
        /// <param name="locator"></param>
        /// <returns></returns>
        private static List<String> GetTextFromElements2(By locator)
        {
            WaitForElementToBePresentInDom(locator);
            var elements = FindElements(locator);
            return elements.Select(element => element.Text).ToList();
        }

        public static string GetPageSource()
        {
            return GetDriver().PageSource;
        }

        public static void UploadFileWithName(string fileName)
        {
            Thread.Sleep(1000);
            Clipboard.SetText(fileName);
            KeyboardSimulator.KeyDown(Keys.Control);
            KeyboardSimulator.KeyPress(Keys.V);
            KeyboardSimulator.KeyUp(Keys.Control);
            KeyboardSimulator.KeyPress(Keys.Enter);
        }

        public static void HoverOverElement(By locator)
        {
            Actions action = new Actions(GetDriver());
            action.MoveToElement(FindElement(locator), 5, 5).Build().Perform();
        }

        public static void SwitchToNewlyOpenedTab()
        {
            List<string> tabs = new List<string>(GetDriver().WindowHandles);
            GetDriver().SwitchTo().Window(tabs[tabs.Count - 1]);
        }

        public static void SwitchBackToOldTab()
        {
            List<string> tabs = new List<string>(GetDriver().WindowHandles);
            GetDriver().Close();
            GetDriver().SwitchTo().Window(tabs[tabs.Count - 2]);    
        }

        public static string GetUrl()
        {
            return GetDriver().Url;
        }

        /// <summary>
        /// To scroll down enter positive value, to scroll up enter negative value;
        /// </summary>
        /// <param name="pixels">Number of pixels to scroll.</param>
        public static void ScrollPage(int pixels)
        {
            var js = GetDriver() as IJavaScriptExecutor;
            js.ExecuteScript("window.scrollBy(0," + pixels + ")", "");
        }

        /// <summary>
        /// To scroll down enter positive value, to scroll up enter negative value;
        /// </summary>
        /// <param name="elementId">Id of element which will be scrolled.</param>
        /// <param name="pixels">Number of pixels to scroll.</param>
        public static void ScrollModalWindow(string elementId, int pixels)
        {
            var js = GetDriver() as IJavaScriptExecutor;
            js.ExecuteScript("document.querySelector('#" + elementId + "').scrollTop += " + pixels);
        }
    }
}
