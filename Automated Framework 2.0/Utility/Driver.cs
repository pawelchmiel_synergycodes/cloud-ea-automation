using Applitools;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Configuration;
using System.Threading;

namespace Automated_Framework_2._0.Utility
{
    public class Driver
    {
        private static Eyes _eyes;
        public static IWebDriver Instance { get; set; }

        public static void Initialize(string appName, string testName)
        {
            var options = new ChromeOptions();
            options.AddArgument("start-maximized");
            var driver = new ChromeDriver(options);
            Instance = GetEyes().Open(driver, appName, testName);
        }

        public static Eyes GetEyes()
        {
            if (_eyes != null) return _eyes;
            _eyes = new Eyes { ApiKey = ConfigurationManager.AppSettings["AppliKey"] };
            _eyes.SaveFailedTests = true;
            return _eyes;
        }

        public static void CleanUp()
        {
            Instance.Quit();
            GetEyes().Close();
        }

        public static void TakeSnapshot(string snapName)
        {
            Thread.Sleep(100);
            GetEyes().CheckWindow(snapName);
        }
    }
}