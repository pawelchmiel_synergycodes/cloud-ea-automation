﻿using System;
using Automated_Framework_2._0.Utility.Enums;

namespace Automated_Framework_2._0.Utility
{
    public class App
    {
        public string AppFramework { get; set; }
        public int AppLeftMenuIndexOnDashboard { get; set; }
        public int AppMainMenuIndexOnDashboard { get; set; }
        public string AppName { get; set; }
        public string AppProjectNameId { get; set; }
        /*
         AppProjectNameId - defines ID of element that contains current name of viewpoint in app or name of project in diagramming apps.
         Possible values:
         - CurrentProjectNameRibbon(Diagramming, Matrices, BPMN, OCGD)
         - CurrentCatalogTypeRibbon(Catalogs)
         - CurrentDeliverableTypeRibbon(Deliverables)
         */

        public App(AppsNames appName)
        {
            switch (appName)
            {
                ////////////////////////////////////////////////////////////////// DODAF Apps
                case AppsNames.DodafViewpoints:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 0;
                    AppMainMenuIndexOnDashboard = 0;
                    AppName = "DoDAF Viewpoints";
                    AppProjectNameId = null;
                    break;

                case AppsNames.DodafFfpCatalogs:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 1;
                    AppMainMenuIndexOnDashboard = 0;
                    AppName = "DoDAF Catalogs";
                    AppProjectNameId = "CurrentCatalogTypeRibbon";
                    break;

                case AppsNames.DodafFfpMatrices:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 1;
                    AppMainMenuIndexOnDashboard = 1;
                    AppName = "DoDAF Matrices";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.DodafFfpDiagramming:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 1;
                    AppMainMenuIndexOnDashboard = 2;
                    AppName = "DoDAF Diagrams";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.DodafFfpDeliverables:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 1;
                    AppMainMenuIndexOnDashboard = 3;
                    AppName = "DoDAF Deliverable";
                    AppProjectNameId = "CurrentDeliverableTypeRibbon";
                    break;

                case AppsNames.DodafFfpBpmn:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 1;
                    AppMainMenuIndexOnDashboard = 4;
                    AppName = "Business Process Modeling";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.DodafJcids:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 2;
                    AppMainMenuIndexOnDashboard = 0;
                    AppName = "JCIDS";
                    AppProjectNameId = null;
                    break;

                case AppsNames.DodafReferenceModelManagementApp:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 3;
                    AppMainMenuIndexOnDashboard = 0;
                    AppName = "Reference Model Management";
                    AppProjectNameId = null;
                    break;

                case AppsNames.DodafArtifactViewer:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 4;
                    AppMainMenuIndexOnDashboard = 0;
                    AppName = "Artifact Viewer";
                    AppProjectNameId = null;
                    break;

                case AppsNames.DodafAv1:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 5;
                    AppMainMenuIndexOnDashboard = 0;
                    AppName = "AV-1 Overview and Summary Information";
                    AppProjectNameId = "CurrentDeliverableTypeRibbon";
                    break;

                case AppsNames.DodafAv2:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 5;
                    AppMainMenuIndexOnDashboard = 1;
                    AppName = "AV-2 Integrated Dictionary";
                    AppProjectNameId = "CurrentCatalogTypeRibbon";
                    break;

                case AppsNames.DodafCv1:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 6;
                    AppMainMenuIndexOnDashboard = 0;
                    AppName = "CV-1 Vision";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.DodafCv2:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 6;
                    AppMainMenuIndexOnDashboard = 1;
                    AppName = "CV-2 Capability Taxonomy";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.DodafCv3:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 6;
                    AppMainMenuIndexOnDashboard = 2;
                    AppName = "CV-3 Capability Phasing";
                    AppProjectNameId = "Scheduling";
                    break;

                case AppsNames.DodafCv4:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 6;
                    AppMainMenuIndexOnDashboard = 3;
                    AppName = "CV-4 Capability Dependencies";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.DodafCv5:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 6;
                    AppMainMenuIndexOnDashboard = 4;
                    AppName = "CV-5 Capability to Organizational Development Mapping";
                    AppProjectNameId = "Scheduling";
                    break;

                case AppsNames.DodafCv6:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 6;
                    AppMainMenuIndexOnDashboard = 5;
                    AppName = "CV-6 Capability to Operational Activities Mapping";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.DodafCv7:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 6;
                    AppMainMenuIndexOnDashboard = 6;
                    AppName = "CV-7 Capability to Services Mapping";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.DodafDiv1:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 7;
                    AppMainMenuIndexOnDashboard = 0;
                    AppName = "DIV-1 Conceptual Data Model";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.DodafDiv2:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 7;
                    AppMainMenuIndexOnDashboard = 1;
                    AppName = "DIV-2 Logical Data Model";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.DodafDiv3:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 7;
                    AppMainMenuIndexOnDashboard = 2;
                    AppName = "DIV-3 Physical Data Model";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.DodafOv1:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 8;
                    AppMainMenuIndexOnDashboard = 0;
                    AppName = "OCGD";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.DodafOv2:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 8;
                    AppMainMenuIndexOnDashboard = 1;
                    AppName = "OV-2 Operational Resource Flow Description";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.DodafOv3:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 8;
                    AppMainMenuIndexOnDashboard = 2;
                    AppName = "OV-3 Operational Resource Flow Matrix";
                    AppProjectNameId = "CurrentCatalogTypeRibbon";
                    break;

                case AppsNames.DodafOv4:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 8;
                    AppMainMenuIndexOnDashboard = 3;
                    AppName = "OV-4 Organizational Relationships Chart";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.DodafOv5a:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 8;
                    AppMainMenuIndexOnDashboard = 4;
                    AppName = "OV-5a Operational Activity Decomposition Tree";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.DodafOv5b:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 8;
                    AppMainMenuIndexOnDashboard = 5;
                    AppName = "OV-5b Operational Activity Model";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.DodafOv6a:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 8;
                    AppMainMenuIndexOnDashboard = 6;
                    AppName = "OV-6a Operational Rules Model";
                    AppProjectNameId = "CurrentCatalogTypeRibbon";
                    break;

                case AppsNames.DodafOv6b:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 8;
                    AppMainMenuIndexOnDashboard = 7;
                    AppName = "OV-6b State Transition Description";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.DodafOv6c:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 8;
                    AppMainMenuIndexOnDashboard = 8;
                    AppName = "OV-6c Event-Trace Description";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.DodafPv1:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 9;
                    AppMainMenuIndexOnDashboard = 0;
                    AppName = "PV-1 Project Portfolio Relationships";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.DodafPv2:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 9;
                    AppMainMenuIndexOnDashboard = 1;
                    AppName = "PV-2 Project Timelines";
                    AppProjectNameId = "Scheduling";
                    break;

                case AppsNames.DodafPv3:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 9;
                    AppMainMenuIndexOnDashboard = 2;
                    AppName = "PV-3 Project to Capability Mapping";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.DodafSvcv1:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 10;
                    AppMainMenuIndexOnDashboard = 0;
                    AppName = "SvcV-1 Services Context Description";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.DodafSvcv2:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 10;
                    AppMainMenuIndexOnDashboard = 1;
                    AppName = "SvcV-2 Services Resource Flow Description";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.DodafSvcv3a:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 10;
                    AppMainMenuIndexOnDashboard = 2;
                    AppName = "SvcV-3a Systems-Services Matrix";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.DodafSvcv3b:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 10;
                    AppMainMenuIndexOnDashboard = 3;
                    AppName = "SvcV-3b Services-Services Matrix";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.DodafSvcv4a:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 10;
                    AppMainMenuIndexOnDashboard = 4;
                    AppName = "SvcV-4a Service Decomposition";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.DodafSvcv4b:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 10;
                    AppMainMenuIndexOnDashboard = 5;
                    AppName = "SvcV-4b Service Flow";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.DodafSvcv5:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 10;
                    AppMainMenuIndexOnDashboard = 6;
                    AppName = "SvcV-5 Operational Activity to Services Traceability Matrix";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.DodafSvcv6:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 10;
                    AppMainMenuIndexOnDashboard = 7;
                    AppName = "SvcV-6 Services Resource Flow Matrix";
                    AppProjectNameId = "CurrentCatalogTypeRibbon";
                    break;

                case AppsNames.DodafSvcv7:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 10;
                    AppMainMenuIndexOnDashboard = 8;
                    AppName = "SvcV-7 Services Measures Matrix";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.DodafSvcv8:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 10;
                    AppMainMenuIndexOnDashboard = 9;
                    AppName = "SvcV-8 Services Evolution Description";
                    AppProjectNameId = "Scheduling";
                    break;

                case AppsNames.DodafSvcv9:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 10;
                    AppMainMenuIndexOnDashboard = 10;
                    AppName = "SvcV-9 Services Technology and Skills Forecast";
                    AppProjectNameId = "CurrentCatalogTypeRibbon";
                    break;

                case AppsNames.DodafSvcv10a:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 10;
                    AppMainMenuIndexOnDashboard = 11;
                    AppName = "SvcV-10a Services Rules Model";
                    AppProjectNameId = "CurrentCatalogTypeRibbon";
                    break;

                case AppsNames.DodafSvcv10b:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 10;
                    AppMainMenuIndexOnDashboard = 12;
                    AppName = "SvcV-10b Services State Transition Description";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.DodafSvcv10c:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 10;
                    AppMainMenuIndexOnDashboard = 13;
                    AppName = "SvcV-10c Services Event-Trace Description";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.DodafStdv1:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 11;
                    AppMainMenuIndexOnDashboard = 0;
                    AppName = "StdV-1 Standards Profile";
                    AppProjectNameId = "CurrentCatalogTypeRibbon";
                    break;

                case AppsNames.DodafStdv2:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 11;
                    AppMainMenuIndexOnDashboard = 1;
                    AppName = "StdV-1 Standards Forecast";
                    AppProjectNameId = "CurrentCatalogTypeRibbon";
                    break;

                case AppsNames.DodafSv1:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 12;
                    AppMainMenuIndexOnDashboard = 0;
                    AppName = "SV-1 Systems Interface Description";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.DodafSv2:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 12;
                    AppMainMenuIndexOnDashboard = 1;
                    AppName = "SV-2 Systems Resource Flow Description";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.DodafSv3:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 12;
                    AppMainMenuIndexOnDashboard = 2;
                    AppName = "SV-3 Systems-Systems Matrix";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.DodafSv4a:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 12;
                    AppMainMenuIndexOnDashboard = 3;
                    AppName = "SV-4a Functional Decomposition";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.DodafSv4b:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 12;
                    AppMainMenuIndexOnDashboard = 4;
                    AppName = "SV-4b Data Flow";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.DodafSv5a:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 12;
                    AppMainMenuIndexOnDashboard = 5;
                    AppName = "SV-5a Operational Activity to Systems Function Traceability Matrix";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.DodafSv5b:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 12;
                    AppMainMenuIndexOnDashboard = 6;
                    AppName = "SV-5b Operational Activity to Systems Traceability Matrix";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.DodafSv6:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 12;
                    AppMainMenuIndexOnDashboard = 7;
                    AppName = "SV-6 Systems Resource Flow Matrix";
                    AppProjectNameId = "CurrentCatalogTypeRibbon";
                    break;

                case AppsNames.DodafSv7:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 12;
                    AppMainMenuIndexOnDashboard = 8;
                    AppName = "SV-7 Systems Measures Matrix";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.DodafSv8:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 12;
                    AppMainMenuIndexOnDashboard = 9;
                    AppName = "SV-8 Systems Evolution Description";
                    AppProjectNameId = "Scheduling";
                    break;

                case AppsNames.DodafSv9:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 12;
                    AppMainMenuIndexOnDashboard = 10;
                    AppName = "SV-9 Systems Technology and Skills Forecast";
                    AppProjectNameId = "CurrentCatalogTypeRibbon";
                    break;

                case AppsNames.DodafSv10a:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 12;
                    AppMainMenuIndexOnDashboard = 11;
                    AppName = "SV-10a Systems Rules Model";
                    AppProjectNameId = "CurrentCatalogTypeRibbon";
                    break;

                case AppsNames.DodafSv10b:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 12;
                    AppMainMenuIndexOnDashboard = 12;
                    AppName = "SV-10b Systems State Transition Description";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.DodafSv10c:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 12;
                    AppMainMenuIndexOnDashboard = 13;
                    AppName = "SV-10c Systems Event-Trace Description";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.DodafDocumentAndFileManagementApp:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 13;
                    AppMainMenuIndexOnDashboard = 0;
                    AppName = "Document Management";
                    AppProjectNameId = null;
                    break;

                case AppsNames.DodafLicenseManagementApp:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 14;
                    AppMainMenuIndexOnDashboard = 0;
                    AppName = "LicenseManagementApp";
                    AppProjectNameId = null;
                    break;

                case AppsNames.DodafRepositoryManagementApp:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 15;
                    AppMainMenuIndexOnDashboard = 0;
                    AppName = "Repository Management";
                    AppProjectNameId = null;
                    break;

                case AppsNames.DodafUserAndGroupManagementApp:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 16;
                    AppMainMenuIndexOnDashboard = 0;
                    AppName = "User and Group Management";
                    AppProjectNameId = null;
                    break;
//////////////////////////////////////////////////////////////////// reports not supported yet
                case AppsNames.DodafArchitectureComplianceReport:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 16;
                    AppMainMenuIndexOnDashboard = 0;
                    AppName = "Architecture Compliance Report";
                    AppProjectNameId = null;
                    break;

                case AppsNames.DodafOverviewAndSummaryReport:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 16;
                    AppMainMenuIndexOnDashboard = 1;
                    AppName = "Overview Summary Report";
                    AppProjectNameId = null;
                    break;

                case AppsNames.DodafArchitectureAlignmentReport:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 16;
                    AppMainMenuIndexOnDashboard = 2;
                    AppName = "Alignment Report";
                    AppProjectNameId = null;
                    break;

                case AppsNames.DodafArchitectureComparsionReport:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 16;
                    AppMainMenuIndexOnDashboard = 3;
                    AppName = "Comparison Report";
                    AppProjectNameId = null;
                    break;

                case AppsNames.DodafArchitectureCompletenessReport:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 16;
                    AppMainMenuIndexOnDashboard = 4;
                    AppName = "Completeness Report";
                    AppProjectNameId = null;
                    break;

                case AppsNames.DodafGapAnalysisReport:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 17;
                    AppMainMenuIndexOnDashboard = 0;
                    AppName = "Gap Analysis Report";
                    AppProjectNameId = null;
                    break;

                case AppsNames.DodafInteroperabilityAnalysisReport:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 17;
                    AppMainMenuIndexOnDashboard = 1;
                    AppName = "Interoperability Analysis Report";
                    AppProjectNameId = null;
                    break;

                case AppsNames.DodafImpactAnalysisReport:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 17;
                    AppMainMenuIndexOnDashboard = 2;
                    AppName = "Impact Analysis Report";
                    AppProjectNameId = null;
                    break;

                case AppsNames.DodafLineOfSightAnalysisReport:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 17;
                    AppMainMenuIndexOnDashboard = 3;
                    AppName = "Line of Sight Report";
                    AppProjectNameId = null;
                    break;

                case AppsNames.DodafComponentCountAnalysisReport:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 17;
                    AppMainMenuIndexOnDashboard = 4;
                    AppName = "Component Count Report";
                    AppProjectNameId = null;
                    break;

                case AppsNames.DodafRoadmapReport:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 18;
                    AppMainMenuIndexOnDashboard = 0;
                    AppName = "Roadmap Report";
                    AppProjectNameId = null;
                    break;

                case AppsNames.DodafEnterpriseComplianceReport:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 19;
                    AppMainMenuIndexOnDashboard = 0;
                    AppName = "Enterprise Compliance Report";
                    AppProjectNameId = null;
                    break;

                case AppsNames.DodafEnterpriseCompletenessReport:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 19;
                    AppMainMenuIndexOnDashboard = 1;
                    AppName = "Enterprise Completeness Report";
                    AppProjectNameId = null;
                    break;

                case AppsNames.DodafEnterpriseAlignmentReport:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 19;
                    AppMainMenuIndexOnDashboard = 2;
                    AppName = "Enterprise Alignment Report";
                    AppProjectNameId = null;
                    break;

                case AppsNames.DodafEnterpriseReferenceReport:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 19;
                    AppMainMenuIndexOnDashboard = 3;
                    AppName = "Enterprise Reference Report";
                    AppProjectNameId = null;
                    break;

                case AppsNames.DodafArchitectureConsolidationReport:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 19;
                    AppMainMenuIndexOnDashboard = 4;

                    AppName = null;
                    AppProjectNameId = null;
                    break;

                case AppsNames.DodafArchitectureOptimizationReport:
                    AppFramework = "DoDAF";
                    AppLeftMenuIndexOnDashboard = 19;
                    AppMainMenuIndexOnDashboard = 5;
                    AppName = null;
                    AppProjectNameId = null;
                    break;

                //////////////////////////////////////////////////////////////////FEAF Apps
                case AppsNames.FeafCpm:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 0;
                    AppMainMenuIndexOnDashboard = 0;
                    AppName = "CPM";
                    AppProjectNameId = null;
                    break;

                case AppsNames.FeafCube:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 1;
                    AppMainMenuIndexOnDashboard = 0;
                    AppName = "Sub-Architecture Domains";
                    AppProjectNameId = null;
                    break;

                case AppsNames.FeafCatalogs:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 2;
                    AppMainMenuIndexOnDashboard = 0;
                    AppName = "Catalogs";
                    AppProjectNameId = null;
                    break;

                case AppsNames.FeafMatrices:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 2;
                    AppMainMenuIndexOnDashboard = 1;
                    AppName = "Matrices";
                    AppProjectNameId = null;
                    break;

                case AppsNames.FeafDeliverables:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 2;
                    AppMainMenuIndexOnDashboard = 2;
                    AppName = "Deliverable";
                    AppProjectNameId = null;
                    break;

                case AppsNames.FeafDiagramming:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 2;
                    AppMainMenuIndexOnDashboard = 3;
                    AppName = "FEAF Diagrams";
                    AppProjectNameId = null;
                    break;

                case AppsNames.FeafArtifactViewer:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 3;
                    AppMainMenuIndexOnDashboard = 0;
                    AppName = "Artifact Viewer";
                    AppProjectNameId = null;
                    break;

                case AppsNames.FeafReferenceModelManagementApp:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 4;
                    AppMainMenuIndexOnDashboard = 0;
                    AppName = "Reference Model Management";
                    AppProjectNameId = null;
                    break;

                case AppsNames.FeafS1:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 5;
                    AppMainMenuIndexOnDashboard = 0;
                    AppName = "S-1 Concept Overview Diagram (core)";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.FeafS2:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 5;
                    AppMainMenuIndexOnDashboard = 1;
                    AppName = "S-2 Strategic Plan";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.FeafS3:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 5;
                    AppMainMenuIndexOnDashboard = 2;
                    AppName = "S-3 Concept of Operations Scenarios";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.FeafS4:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 5;
                    AppMainMenuIndexOnDashboard = 3;
                    AppName = "S-4 SWOT Analysis";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.FeafS5:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 5;
                    AppMainMenuIndexOnDashboard = 4;
                    AppName = "S-5 Performance Measures Scorecard Deliverable";
                    AppProjectNameId = "CurrentDeliverableTypeRibbon";
                    break;

                case AppsNames.FeafB1:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 6;
                    AppMainMenuIndexOnDashboard = 0;
                    AppName = "B-1 Business Process Diagram (core)";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.FeafB2:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 6;
                    AppMainMenuIndexOnDashboard = 1;
                    AppName = "B-2 Business Operating Plan";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.FeafB3:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 6;
                    AppMainMenuIndexOnDashboard = 2;
                    AppName = "B-3 Business Service Catalog";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.FeafB4:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 6;
                    AppMainMenuIndexOnDashboard = 3;
                    AppName = "B-4 Organization Chart";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.FeafB5:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 6;
                    AppMainMenuIndexOnDashboard = 4;
                    AppName = "B-5 Use case AppsNames. Narrative and Diagram";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.FeafB6:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 6;
                    AppMainMenuIndexOnDashboard = 5;
                    AppName = "B-6 Business case AppsNames. / Alternatives Analysis Deliverable";
                    AppProjectNameId = "CurrentDeliverableTypeRibbon";
                    break;

                case AppsNames.FeafD1:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 7;
                    AppMainMenuIndexOnDashboard = 0;
                    AppName = "D-1 Logical Data Model (core)";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.FeafD2:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 7;
                    AppMainMenuIndexOnDashboard = 1;
                    AppName = "D-2 Knowledge Management Plan Deliverable";
                    AppProjectNameId = "CurrentDeliverableTypeRibbon";
                    break;

                case AppsNames.FeafD3:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 7;
                    AppMainMenuIndexOnDashboard = 2;
                    AppName = "D-3 Data Quality Plan Deliverable";
                    AppProjectNameId = "CurrentDeliverableTypeRibbon";
                    break;

                case AppsNames.FeafD4:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 7;
                    AppMainMenuIndexOnDashboard = 3;
                    AppName = "D-4 Data Flow Diagram";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.FeafD5:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 7;
                    AppMainMenuIndexOnDashboard = 4;
                    AppName = "D-5 Physical Data Model";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.FeafD6:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 7;
                    AppMainMenuIndexOnDashboard = 5;
                    AppName = "D-6 CRUD Matrix";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.FeafD7:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 7;
                    AppMainMenuIndexOnDashboard = 6;
                    AppName = "D-7 State-Transition Diagram";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.FeafD8:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 7;
                    AppMainMenuIndexOnDashboard = 7;
                    AppName = "D-8 Event Sequence Diagram";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.FeafD9:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 7;
                    AppMainMenuIndexOnDashboard = 8;
                    AppName = "D-9 Data Dictionary";
                    AppProjectNameId = "CurrentCatalogTypeRibbon";
                    break;

                case AppsNames.FeafD10:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 7;
                    AppMainMenuIndexOnDashboard = 9;
                    AppName = "D-10 Object Library";
                    AppProjectNameId = "CurrentCatalogTypeRibbon";
                    break;

                case AppsNames.FeafA1:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 8;
                    AppMainMenuIndexOnDashboard = 0;
                    AppName = "A-1 Application Interface Diagram (core)";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.FeafA2:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 8;
                    AppMainMenuIndexOnDashboard = 1;
                    AppName = "A-2 Application Communication Diagram";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.FeafA3:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 8;
                    AppMainMenuIndexOnDashboard = 2;
                    AppName = "A-3 Application Interface Matrix";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.FeafA4:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 8;
                    AppMainMenuIndexOnDashboard = 3;
                    AppName = "A-4 Application Data Exchange";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.FeafA5:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 8;
                    AppMainMenuIndexOnDashboard = 4;
                    AppName = "A-5 Application Service Matrix";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.FeafA6:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 8;
                    AppMainMenuIndexOnDashboard = 5;
                    AppName = "A-6 Application Performance Matrix";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.FeafA7:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 8;
                    AppMainMenuIndexOnDashboard = 6;
                    AppName = "A-7 System/Application Evolution Diagram";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.FeafA8:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 8;
                    AppMainMenuIndexOnDashboard = 7;
                    AppName = "A-8 Enterprise Service Bus Diagram";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.FeafA9:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 8;
                    AppMainMenuIndexOnDashboard = 8;
                    AppName = "A-9 Application Maintenance Procedure Deliverable";
                    AppProjectNameId = "CurrentDeliverableTypeRibbon";
                    break;

                case AppsNames.FeafA10:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 8;
                    AppMainMenuIndexOnDashboard = 9;
                    AppName = "A-10 Application Inventory";
                    AppProjectNameId = "CurrentCatalogTypeRibbon";
                    break;

                case AppsNames.FeafA11:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 8;
                    AppMainMenuIndexOnDashboard = 10;
                    AppName = "A-11 Software License Inventory";
                    AppProjectNameId = "CurrentCatalogTypeRibbon";
                    break;

                case AppsNames.FeafI1:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 9;
                    AppMainMenuIndexOnDashboard = 0;
                    AppName = "Document Management";
                    AppProjectNameId = null;
                    break;

                case AppsNames.FeafI2:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 9;
                    AppMainMenuIndexOnDashboard = 1;
                    AppName = "I-2 Hosting Concept of Operations Deliverable";
                    AppProjectNameId = "CurrentDeliverableTypeRibbon";
                    break;

                case AppsNames.FeafI3:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 9;
                    AppMainMenuIndexOnDashboard = 2;
                    AppName = "I-3 Technical Standards Profile";
                    AppProjectNameId = "CurrentCatalogTypeRibbon";
                    break;

                case AppsNames.FeafI4:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 9;
                    AppMainMenuIndexOnDashboard = 3;
                    AppName = "I-4 Technology Forecast Deliverable";
                    AppProjectNameId = "CurrentDeliverableTypeRibbon";
                    break;

                case AppsNames.FeafI5:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 9;
                    AppMainMenuIndexOnDashboard = 4;
                    AppName = "Document Management";
                    AppProjectNameId = null;
                    break;

                case AppsNames.FeafI6:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 9;
                    AppMainMenuIndexOnDashboard = 5;
                    AppName = "Document Management";
                    AppProjectNameId = null;
                    break;

                case AppsNames.FeafI7:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 9;
                    AppMainMenuIndexOnDashboard = 6;
                    AppName = "Document Management";
                    AppProjectNameId = null;
                    break;

                case AppsNames.FeafI8:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 9;
                    AppMainMenuIndexOnDashboard = 7;
                    AppName = "Document Management";
                    AppProjectNameId = null;
                    break;

                case AppsNames.FeafI9:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 9;
                    AppMainMenuIndexOnDashboard = 8;
                    AppName = "Document Management";
                    AppProjectNameId = null;
                    break;

                case AppsNames.FeafI10:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 9;
                    AppMainMenuIndexOnDashboard = 9;
                    AppName = "Document Management";
                    AppProjectNameId = null;
                    break;

                case AppsNames.FeafI11:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 9;
                    AppMainMenuIndexOnDashboard = 10;
                    AppName = "I-11 Asset Inventory";
                    AppProjectNameId = "CurrentCatalogTypeRibbon";
                    break;

                case AppsNames.FeafI12:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 9;
                    AppMainMenuIndexOnDashboard = 11;
                    AppName = "Document Management";
                    AppProjectNameId = null;
                    break;

                case AppsNames.FeafSp1:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 10;
                    AppMainMenuIndexOnDashboard = 0;
                    AppName = "SP-1 Security Controls Catalog (core)";
                    AppProjectNameId = "CurrentCatalogTypeRibbon";
                    break;

                case AppsNames.FeafSp2:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 10;
                    AppMainMenuIndexOnDashboard = 1;
                    AppName = "SP-2 Security & Accreditation Documentation";
                    AppProjectNameId = "CurrentDeliverableTypeRibbon";
                    break;

                case AppsNames.FeafSp3:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 10;
                    AppMainMenuIndexOnDashboard = 2;
                    AppName = "SP-3 Security Authorization Documentation Deliverable";
                    AppProjectNameId = "CurrentDeliverableTypeRibbon";
                    break;

                case AppsNames.FeafSp3_1:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 10;
                    AppMainMenuIndexOnDashboard = 3;
                    AppName = "SP-3.1 System Security Plan";
                    AppProjectNameId = "CurrentDeliverableTypeRibbon";
                    break;

                case AppsNames.FeafSp3_2:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 10;
                    AppMainMenuIndexOnDashboard = 4;
                    AppName = "SP-3.2 Security Assessment Report";
                    AppProjectNameId = "CurrentDeliverableTypeRibbon";
                    break;

                case AppsNames.FeafSp3_3:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 10;
                    AppMainMenuIndexOnDashboard = 5;
                    AppName = "SP-3.3 POAM";
                    AppProjectNameId = "CurrentDeliverableTypeRibbon";
                    break;

                case AppsNames.FeafSp4:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 10;
                    AppMainMenuIndexOnDashboard = 6;
                    AppName = "SP-4 Continuous Monitoring Procedures Deliverable";
                    AppProjectNameId = "CurrentDeliverableTypeRibbon";
                    break;

                case AppsNames.FeafSp5:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 10;
                    AppMainMenuIndexOnDashboard = 7;
                    AppName = "SP-5 Disaster Recovery Plan Deliverable";
                    AppProjectNameId = "CurrentDeliverableTypeRibbon";
                    break;

                case AppsNames.FeafSp6:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 10;
                    AppMainMenuIndexOnDashboard = 8;
                    AppName = "SP-6 Continuity of Operations Plan Deliverable";
                    AppProjectNameId = "CurrentDeliverableTypeRibbon";
                    break;

                case AppsNames.FeafDocumentAndFileManagementApp:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 11;
                    AppMainMenuIndexOnDashboard = 0;
                    AppName = "Document Management";
                    AppProjectNameId = null;
                    break;

                case AppsNames.FeafLicenseManagementApp:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 12;
                    AppMainMenuIndexOnDashboard = 0;
                    AppName = "LicenseManagementApp";
                    AppProjectNameId = null;
                    break;

                case AppsNames.FeafRepositoryManagementApp:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 13;
                    AppMainMenuIndexOnDashboard = 0;
                    AppName = "Repository Management";
                    AppProjectNameId = null;
                    break;

                case AppsNames.FeafUserAndGroupManagementApp:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 14;
                    AppMainMenuIndexOnDashboard = 0;
                    AppName = "User and Group Management";
                    AppProjectNameId = null;
                    break;

                case AppsNames.FeafArchitectureComplianceReport:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 15;
                    AppMainMenuIndexOnDashboard = 0;
                    AppName = "Architecture Compliance Report";
                    AppProjectNameId = null;
                    break;

                case AppsNames.FeafOverviewAndSummaryReport:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 15;
                    AppMainMenuIndexOnDashboard = 1;
                    AppName = "Overview Summary Report";
                    AppProjectNameId = null;
                    break;

                case AppsNames.FeafArchitectureAlignmentReport:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 15;
                    AppMainMenuIndexOnDashboard = 2;
                    AppName = "Alignment Report";
                    AppProjectNameId = null;
                    break;

                case AppsNames.FeafArchitectureComparsionReport:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 15;
                    AppMainMenuIndexOnDashboard = 3;
                    AppName = "Comparison Report";
                    AppProjectNameId = null;
                    break;

                case AppsNames.FeafArchitectureCompletenessReport:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 15;
                    AppMainMenuIndexOnDashboard = 4;
                    AppName = "Completeness Report";
                    AppProjectNameId = null;
                    break;

                case AppsNames.FeafGapAnalysisReport:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 16;
                    AppMainMenuIndexOnDashboard = 0;
                    AppName = "Gap Analysis Report";
                    AppProjectNameId = null;
                    break;

                case AppsNames.FeafInteroperabilityAnalysisReport:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 16;
                    AppMainMenuIndexOnDashboard = 1;
                    AppName = "Interoperability Analysis Report";
                    AppProjectNameId = null;
                    break;

                case AppsNames.FeafImpactAnalysisReport:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 16;
                    AppMainMenuIndexOnDashboard = 2;
                    AppName = "Impact Analysis Report";
                    AppProjectNameId = null;
                    break;

                case AppsNames.FeafLineOfSightAnalysisReport:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 16;
                    AppMainMenuIndexOnDashboard = 3;
                    AppName = "Line of Sight Report";
                    AppProjectNameId = null;
                    break;

                case AppsNames.FeafComponentCountAnalysisReport:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 16;
                    AppMainMenuIndexOnDashboard = 4;
                    AppName = "Component Count Report";
                    AppProjectNameId = null;
                    break;

                case AppsNames.FeafIntegratedPlanReport:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 17;
                    AppMainMenuIndexOnDashboard = 0;
                    AppName = "Integrated Plan";
                    AppProjectNameId = null;
                    break;

                case AppsNames.FeafRoadmapReport:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 17;
                    AppMainMenuIndexOnDashboard = 1;
                    AppName = "Roadmap Report";
                    AppProjectNameId = null;
                    break;

                case AppsNames.FeafEnterpriseComplianceReport:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 18;
                    AppMainMenuIndexOnDashboard = 0;
                    AppName = "Enterprise Compliance Report";
                    AppProjectNameId = null;
                    break;

                case AppsNames.FeafEnterpriseCompletenessReport:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 18;
                    AppMainMenuIndexOnDashboard = 1;
                    AppName = "Enterprise Completeness Report";
                    AppProjectNameId = null;
                    break;

                case AppsNames.FeafEnterpriseAlignmentReport:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 18;
                    AppMainMenuIndexOnDashboard = 2;
                    AppName = "Enterprise Alignment Report";
                    AppProjectNameId = null;
                    break;

                case AppsNames.FeafEnterpriseReferenceReport:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 18;
                    AppMainMenuIndexOnDashboard = 3;
                    AppName = "Enterprise Reference Report";
                    AppProjectNameId = null;
                    break;

                case AppsNames.FeafArchitectureConsolidationReport:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 18;
                    AppMainMenuIndexOnDashboard = 4;

                    AppName = null;
                    AppProjectNameId = null;
                    break;

                case AppsNames.FeafArchitectureOptimizationReport:
                    AppFramework = "FEAF";
                    AppLeftMenuIndexOnDashboard = 18;
                    AppMainMenuIndexOnDashboard = 5;
                    AppName = null;
                    AppProjectNameId = null;
                    break;

                //////////////////////////////////////////////////////////////////TOGAF Apps
                case AppsNames.TogafAdm:
                    AppFramework = "TOGAF";
                    AppLeftMenuIndexOnDashboard = 0;
                    AppMainMenuIndexOnDashboard = 0;
                    AppName = "ADM App";
                    AppProjectNameId = null;
                    break;

                case AppsNames.TogafCatalogs:
                    AppFramework = "TOGAF";
                    AppLeftMenuIndexOnDashboard = 1;
                    AppMainMenuIndexOnDashboard = 0;
                    AppName = "Catalogs";
                    AppProjectNameId = "CurrentCatalogTypeRibbon";
                    break;

                case AppsNames.TogafMatrices:
                    AppFramework = "TOGAF";
                    AppLeftMenuIndexOnDashboard = 1;
                    AppMainMenuIndexOnDashboard = 1;
                    AppName = "Matrices";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.TogafDiagramming:
                    AppFramework = "TOGAF";
                    AppLeftMenuIndexOnDashboard = 1;
                    AppMainMenuIndexOnDashboard = 2;
                    AppName = "TOGAF Diagrams";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.TogafDeliverables:
                    AppFramework = "TOGAF";
                    AppLeftMenuIndexOnDashboard = 1;
                    AppMainMenuIndexOnDashboard = 3;
                    AppName = "Deliverable";
                    AppProjectNameId = "CurrentDeliverableTypeRibbon";
                    break;

                case AppsNames.TogafBpmn:
                    AppFramework = "TOGAF";
                    AppLeftMenuIndexOnDashboard = 2;
                    AppMainMenuIndexOnDashboard = 0;
                    AppName = "BPMN Process Flow Diagram";
                    AppProjectNameId = "CurrentProjectNameRibbon";
                    break;

                case AppsNames.TogafReferenceModelManagementApp:
                    AppFramework = "TOGAF";
                    AppLeftMenuIndexOnDashboard = 3;
                    AppMainMenuIndexOnDashboard = 0;
                    AppName = "Reference Model Management";
                    AppProjectNameId = null;
                    break;

                case AppsNames.TogafArtifactViewer:
                    AppFramework = "TOGAF";
                    AppLeftMenuIndexOnDashboard = 4;
                    AppMainMenuIndexOnDashboard = 0;
                    AppName = "Artifact Viewer";
                    AppProjectNameId = null;
                    break;

                case AppsNames.TogafDocumentAndFileManagementApp:
                    AppFramework = "TOGAF";
                    AppLeftMenuIndexOnDashboard = 5;
                    AppMainMenuIndexOnDashboard = 0;
                    AppName = "Document Management";
                    AppProjectNameId = null;
                    break;

                case AppsNames.TogafLicenseManagementApp:
                    AppFramework = "TOGAF";
                    AppLeftMenuIndexOnDashboard = 6;
                    AppMainMenuIndexOnDashboard = 0;
                    AppName = "LicenseManagementApp";
                    AppProjectNameId = null;
                    break;

                case AppsNames.TogafProjectManagementApp:
                    AppFramework = "TOGAF";
                    AppLeftMenuIndexOnDashboard = 7;
                    AppMainMenuIndexOnDashboard = 0;
                    AppName = "Project Plan";
                    AppProjectNameId = null;
                    break;

                case AppsNames.TogafRepositoryManagementApp:
                    AppFramework = "TOGAF";
                    AppLeftMenuIndexOnDashboard = 8;
                    AppMainMenuIndexOnDashboard = 0;
                    AppName = "Repository Management";
                    AppProjectNameId = null;
                    break;

                case AppsNames.TogafUserAndGroupManagementApp:
                    AppFramework = "TOGAF";
                    AppLeftMenuIndexOnDashboard = 9;
                    AppMainMenuIndexOnDashboard = 0;
                    AppName = "User and Group Management";
                    AppProjectNameId = null;
                    break;

                case AppsNames.TogafArchitectureComplianceReport:
                    AppFramework = "TOGAF";
                    AppLeftMenuIndexOnDashboard = 10;
                    AppMainMenuIndexOnDashboard = 0;
                    AppName = "Architecture Compliance Report";
                    AppProjectNameId = null;
                    break;

                case AppsNames.TogafOverviewAndSummaryReport:
                    AppFramework = "TOGAF";
                    AppLeftMenuIndexOnDashboard = 10;
                    AppMainMenuIndexOnDashboard = 1;
                    AppName = "Overview Summary Report";
                    AppProjectNameId = null;
                    break;

                case AppsNames.TogafArchitectureAlignmentReport:
                    AppFramework = "TOGAF";
                    AppLeftMenuIndexOnDashboard = 10;
                    AppMainMenuIndexOnDashboard = 2;
                    AppName = "Alignment Report";
                    AppProjectNameId = null;
                    break;

                case AppsNames.TogafArchitectureComparsionReport:
                    AppFramework = "TOGAF";
                    AppLeftMenuIndexOnDashboard = 10;
                    AppMainMenuIndexOnDashboard = 3;
                    AppName = "Comparison Report";
                    AppProjectNameId = null;
                    break;

                case AppsNames.TogafArchitectureCompletenessReport:
                    AppFramework = "TOGAF";
                    AppLeftMenuIndexOnDashboard = 10;
                    AppMainMenuIndexOnDashboard = 4;
                    AppName = "Completeness Report";
                    AppProjectNameId = null;
                    break;

                case AppsNames.TogafInteroperabilityAnalysisReport:
                    AppFramework = "TOGAF";
                    AppLeftMenuIndexOnDashboard = 11;
                    AppMainMenuIndexOnDashboard = 0;
                    AppName = "Interoperability Analysis Report";
                    AppProjectNameId = null;
                    break;

                case AppsNames.TogafImpactAnalysisReport:
                    AppFramework = "TOGAF";
                    AppLeftMenuIndexOnDashboard = 11;
                    AppMainMenuIndexOnDashboard = 1;
                    AppName = "Impact Analysis Report";
                    AppProjectNameId = null;
                    break;

                case AppsNames.TogafLineOfSightAnalysisReport:
                    AppFramework = "TOGAF";
                    AppLeftMenuIndexOnDashboard = 11;
                    AppMainMenuIndexOnDashboard = 2;
                    AppName = "Line of Sight Report";
                    AppProjectNameId = null;
                    break;

                case AppsNames.TogafComponentCountAnalysisReport:
                    AppFramework = "TOGAF";
                    AppLeftMenuIndexOnDashboard = 11;
                    AppMainMenuIndexOnDashboard = 3;
                    AppName = "Component Count Report";
                    AppProjectNameId = null;
                    break;

                case AppsNames.TogafGapAnalysisReport:
                    AppFramework = "TOGAF";
                    AppLeftMenuIndexOnDashboard = 11;
                    AppMainMenuIndexOnDashboard = 4;
                    AppName = "Gap Analysis Report";
                    AppProjectNameId = null;
                    break;

                case AppsNames.TogafRoadmapReport:
                    AppFramework = "TOGAF";
                    AppLeftMenuIndexOnDashboard = 12;
                    AppMainMenuIndexOnDashboard = 0;
                    AppName = "Roadmap Report";
                    AppProjectNameId = null;
                    break;

                case AppsNames.TogafEnterpriseComplianceReport:
                    AppFramework = "TOGAF";
                    AppLeftMenuIndexOnDashboard = 13;
                    AppMainMenuIndexOnDashboard = 0;
                    AppName = "Enterprise Compliance Report";
                    AppProjectNameId = null;
                    break;

                case AppsNames.TogafEnterpriseCompletenessReport:
                    AppFramework = "TOGAF";
                    AppLeftMenuIndexOnDashboard = 13;
                    AppMainMenuIndexOnDashboard = 1;
                    AppName = "Enterprise Completeness Report";
                    AppProjectNameId = null;
                    break;

                case AppsNames.TogafEnterpriseAlignmentReport:
                    AppFramework = "TOGAF";
                    AppLeftMenuIndexOnDashboard = 13;
                    AppMainMenuIndexOnDashboard = 2;
                    AppName = "Enterprise Alignment Report";
                    AppProjectNameId = null;
                    break;

                case AppsNames.TogafEnterpriseReferenceReport:
                    AppFramework = "TOGAF";
                    AppLeftMenuIndexOnDashboard = 13;
                    AppMainMenuIndexOnDashboard = 3;
                    AppName = "Enterprise Reference Report";
                    AppProjectNameId = null;
                    break;

                case AppsNames.TogafArchitectureConsolidationReport:
                    AppFramework = "TOGAF";
                    AppLeftMenuIndexOnDashboard = 13;
                    AppMainMenuIndexOnDashboard = 4;

                    AppName = null;
                    AppProjectNameId = null;
                    break;

                case AppsNames.TogafArchitectureOptimizationReport:
                    AppFramework = "TOGAF";
                    AppLeftMenuIndexOnDashboard = 13;
                    AppMainMenuIndexOnDashboard = 5;
                    AppName = null;
                    AppProjectNameId = null;
                    break;

                default:
                    throw new Exception("App name not recognized!");
            }
        }
    }
}
