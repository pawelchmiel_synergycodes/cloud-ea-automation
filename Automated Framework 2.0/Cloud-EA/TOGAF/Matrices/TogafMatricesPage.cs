﻿using System;
using Automated_Framework_2._0.Pages;
using OpenQA.Selenium;

namespace Automated_Framework_2._0.TOGAF_Apps.Matrices
{
    public class TogafMatricesPage : CloudEaPage
    {
        /// <summary>
        /// Method navigates to desired viewpoint.
        /// </summary>
        /// <param name="viewpointName">Viewpoint to which user would like to navigate.</param>
        /// <param name="mode">1 - View Only mode, 2 - Edit mode</param>
        /// <returns></returns>
        public static void NavigateToViewpoint(string viewpointName, int mode) // mode = 1(View Mode), 2(EditMode) 
        {
            int viewpointIndex = 0;
            var elements = GetTextFromElements(By.XPath("//*[@id = 'inputTreeView_CD']/ul/li/ul/li/div/span"));
            for (int i = 0; i < elements.Count; i++)
            {
                if (elements[i] != viewpointName) continue;
                viewpointIndex = i;
                break;
            }

            var viewpoint = By.XPath("(//*[@id = 'inputTreeView_CD']/ul/li/ul/li/div)[" + (viewpointIndex + 1) + "]");
            var viewpointParent =
                By.XPath("(//*[@id = 'inputTreeView_CD']/ul/li/ul/li/div)[" + (viewpointIndex + 1) +
                         "]/../../preceding-sibling::span/img");

            if (!FindElement(viewpointParent).Displayed)
                Click(By.Id("treeNavBar_GHC0"));
            if (!FindElement(viewpoint).Displayed) 
                Click(viewpointParent);

            Click(viewpoint);

            switch (mode)
            {
                case 1:
                    if (CheckOutWindowComponent.CheckIfViewTemplateButtonIsVisible())
                        CheckOutWindowComponent.ClickViewTemplateButton();
                    else
                        CheckOutWindowComponent.ClickViewOnlyButton();
                    break;
                case 2:
                    CheckOutWindowComponent.ClickCheckOutButton();
                    break;
            }
            if (CheckForDataInDataBase("Matrices", viewpointName))
                WaitForComponentInCallback("thumbnails.InCallback()");
        }

        public static string SpreadsheetFirstTabText()
        {
            return SpreadsheetComponent.GetFirstTabText();
        }

        public static void ProcessData(string testData)
        {
            if (!FindElement(By.Id("spreadsheet")).Displayed)
                SpreadsheetComponent.ExpandSpreadsheet();
            SpreadsheetComponent.TypeDataToCell(1, 1, testData);
            SpreadsheetComponent.ProcessSpreadsheet();
        }

        /// <summary>
        /// Saves data to DB using 2 options(Save or Save & Check-In).
        /// </summary>
        /// <param name="mode">1 - Save, 2 - Save & Check-In</param>
        public static void SaveData(int mode)
        {
            switch (mode)
            {
                case 1:
                    SaveWindowComponent.Save("2"); 
                    break;
                case 2:
                    SaveWindowComponent.SaveAndCheckIn("3");
                    break;
                default:
                    throw new Exception("Not recognized mode!");
            }
              
        }

        public static void ConnectToArchitecture(string workspaceName, string architectureName)
        {
            RepositoryWindowComponent.ConnectToArchitecture(workspaceName, architectureName);
        }

        public static void DeleteRelation(string name)
        {
            MatricesGridViewComponent.DeleteRelation(name);
        }

        public static void OpenDataFromRepository(string viewpointName, int mode)
        {
            OpenWindowComponent.Open(viewpointName, mode);
        }
    }
}
