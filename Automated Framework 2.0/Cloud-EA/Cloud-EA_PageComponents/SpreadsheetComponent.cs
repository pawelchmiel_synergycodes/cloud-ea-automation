﻿using System.Threading;
using Automated_Framework_2._0.Pages;
using OpenQA.Selenium;

namespace Automated_Framework_2._0
{
    public class SpreadsheetComponent : CloudEaPage
    {
        private static readonly By SpreadsheetMenuButton = By.Id("spreadsheet_SSRC_TC_T0");
        private static readonly By SpreadsheetProcessButton = By.Id("spreadsheet_SSRC_T0G0I7");
        private static readonly By SpreadsheetNewButton = By.Id("spreadsheet_SSRC_T0G0I5");
        private static readonly By SpreadsheetFirstTab = By.Id("spreadsheet_SSTC_T0T");
        private static readonly By SpreadsheetFirstTabWhenActive = By.Id("spreadsheet_SSTC_AT0T");
        private static readonly By SpreadsheetSecondTab = By.Id("spreadsheet_SSTC_T1T");
        private static readonly By SpreadsheetSecondTabWhenActive = By.Id("spreadsheet_SSTC_AT1T");
        private static readonly By SpreadsheetExpandButton= By.Id("Splitter_1i2_S_CB");
        private static readonly By SpreadsheetCollapseButton = By.Id("Splitter_1i2_S_CF");

        public static void ProcessSpreadsheet()
        {
            Click(SpreadsheetMenuButton);
            Click(SpreadsheetProcessButton);
        }

        public static void ClickNewButton()
        {
            Click(SpreadsheetMenuButton);
            Click(SpreadsheetNewButton);
        }

        public static void CollapseSpreadsheet()
        {
            Click(SpreadsheetCollapseButton);
        }

        public static void ExpandSpreadsheet()
        {
            Click(SpreadsheetExpandButton);
        }

        public static void SwitchToSecondTab()
        {
            Click(SpreadsheetSecondTab);
        }

        public static void SwitchToFirstTab()
        {
            Click(SpreadsheetFirstTab);
        }

        private static By ReturnCellId(int x, int y)
        {
            return By.Id("spreadsheet_ctb." + x + "." + y);
        }

        public static string GetFirstTabText()
        {
            WaitForComponentInCallback("spreadsheet.InCallback()");
            return FindElement(SpreadsheetFirstTabWhenActive).Text;
        }

        public static void TypeDataToCell(int x, int y, string testData)
        {
            Click(ReturnCellId(x, y));
            Thread.Sleep(300);
            GetDriver().SwitchTo().ActiveElement().SendKeys(testData); 
        }
    }
}
