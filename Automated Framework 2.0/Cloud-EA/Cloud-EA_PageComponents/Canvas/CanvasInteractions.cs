﻿ using Automated_Framework_2._0.Pages;
 using OpenQA.Selenium;
 using System.Drawing;
using OpenQA.Selenium.Interactions;
using Automated_Framework_2._0.Cloud_EA.Cloud_EA_PageComponents.Canvas;
using System.Threading;

namespace Automated_Framework_2._0.Cloud_EA.Cloud_EA_PageComponents
{
    public class CanvasInteractions : CloudEaPage
    {
        public static void ClickAndDragOnCanvas(By canvas, Point from, Point to)
        {
            Actions action = new Actions(GetDriver());
            action.MoveToElement(FindElement(canvas), from.X, from.Y).ClickAndHold()
                .MoveToElement(FindElement(canvas), from.X, from.Y + 10)
                .MoveToElement(FindElement(canvas), to.X, to.Y)
                .Release()
                .Build()
                .Perform();
        }

        /// <summary>
        /// Drag and drop element on canvas, can be used to create connections
        /// </summary>
        /// <param name="canvas">The diagram</param>
        /// <param name="from">Start point of drag (mousedown) - offset in pixels from left upper corner of diagram</param>
        /// <param name="to">End point of drag (mouseup) - offset in pixels from left upper corner of diagram</param>
        public static void DragAndDropOnCanvas(By canvas, Point from, Point to)
        {
            Actions action = new Actions(GetDriver());
            action.MoveToElement(FindElement(canvas), from.X, from.Y).ClickAndHold()
                .MoveToElement(FindElement(canvas), to.X, to.Y)
                .Release()
                .Build()
                .Perform();
        }

        /// <summary>
        /// Drag and drop element from palette to diagram
        /// </summary>
        /// <param name="palette">The palette</param>
        /// <param name="diagram">The diagram</param>
        /// <param name="from">Offset in pixels from left upper corner of palette</param>
        /// <param name="to">Offset in pixels from left upper corner of diagram</param>
        public static void DragAndDropFromPaletteToDiagram(By palette, By diagram, Point from, Point to)
        {
            Actions action = new Actions(GetDriver());
            action.MoveToElement(FindElement(palette), from.X, from.Y).ClickAndHold()
                .MoveToElement(FindElement(palette), from.X, from.Y + 10)
                .MoveToElement(FindElement(diagram), to.X, to.Y)
                .Release()
                .Build()
                .Perform();

            // slighty movement on the palette is necessary to initiate drag action, if the movement will be to small
            // or will end outside the palette entire action will have no effect
        }

        /// <summary>
        /// Performs mouse click at a given point in canvas
        /// </summary>
        /// <param name="canvas">The canvas</param>
        /// <param name="point">Offset in pixels from left upper corner of canvas</param>
        public static void ClickOnCanvas(By canvas, Point point)
        {
            Actions action = new Actions(GetDriver());
            action.MoveToElement(FindElement(canvas), point.X, point.Y)
                .Click()
                .Build()
                .Perform();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="palette"></param>
        /// <param name="diagram"></param>
        public static void DragAndDropFromPaletteToDiagramLeftUpperCorner<T>(By palette, By diagram) where T : IGraphObject, new()
        {
            var element = new T();
            Actions action = new Actions(GetDriver());
            action.MoveToElement(FindElement(palette), element.PalettePosition.X, element.PalettePosition.Y).ClickAndHold()
                .MoveToElement(FindElement(palette), element.PalettePosition.X, element.PalettePosition.Y + 10)
                .MoveToElement(FindElement(diagram), 1, 1)
                .Release()
                .Build()
                .Perform();
            Thread.Sleep(100);
        }

        public static void DragAndDropFromPaletteToDiagram<T>(By palette, By diagram, Point to) where T : IGraphObject, new()
        {
            var element = new T();
            Actions action = new Actions(GetDriver());
            action.MoveToElement(FindElement(palette), element.PalettePosition.X, element.PalettePosition.Y).ClickAndHold()
                .MoveToElement(FindElement(palette), element.PalettePosition.X, element.PalettePosition.Y + 10)
                .MoveToElement(FindElement(diagram), to.X, to.Y)
                .Release()
                .Build()
                .Perform();
            Thread.Sleep(100);
        }

        public static void RenameNode(string name, By diagram, Point to)
        {
            Actions action = new Actions(GetDriver());

            // 3 clicks are required to switch from 1 edit name window to another
            action.MoveToElement(FindElement(diagram), to.X, to.Y)
                .Click()
                .Build()
                .Perform();
            Thread.Sleep(200);
            action.Click()
                .Build()
                .Perform();
            Thread.Sleep(200);
            action.Click()
                .Build()
                .Perform(); 
            GetDriver().SwitchTo().ActiveElement().Clear(); // clearing old diagram name
            GetDriver().SwitchTo().ActiveElement().SendKeys(name); // typing in new name
        }
    }
}
