﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automated_Framework_2._0.Cloud_EA.Cloud_EA_PageComponents.Canvas
{
    public interface IGraphObject
    {
        Point PalettePosition { get; }
        Size Size { get; }
    }

    public struct Size
    {
        int Width;
        int Height;

        public Size(int width, int height)
        {
            this.Width = width;
            this.Height = height;
        }
    }
}
