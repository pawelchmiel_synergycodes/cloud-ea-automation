﻿using System.Drawing;

namespace Automated_Framework_2._0.Cloud_EA.Cloud_EA_PageComponents.Canvas.GraphObjects
{
    public class OperationalActivityOv5a : IGraphObject
    {
        public Point PalettePosition
        {
            get { return new Point(200, 60); }
            //get { return new Point(200, 150); } //for fhd screen resolution
        }

        public Size Size
        {
            get { return new Size(105, 70); }
        }
    }
}
