﻿using System.Linq;
using Automated_Framework_2._0.Pages;
using OpenQA.Selenium;

namespace Automated_Framework_2._0
{
    public class RepositoryWindowComponent : CloudEaPage
    {
        private static readonly By RepositoryButtonOnRibbon = By.Id("RepositoryBtn");
        public static void ConnectToArchitecture(string workspaceName, string architectureName)
        {
            Click(RepositoryButtonOnRibbon);
            WaitForElementToBePresentInDom(By.XPath("//*[@id = 'WorkspaceGridPopup1_DXMainTable']/tbody/tr/td[2]"));
            var workspaces = By.XPath("//*[@id = 'WorkspaceGridPopup1_DXMainTable']/tbody/tr/td[2]");
            Click(By.XPath("(//*[@id = 'WorkspaceGridPopup1_DXMainTable']/tbody/tr/td/img)[" 
                + GetElementNumberInElementsArray(workspaces, workspaceName) + "]"));

            WaitForElementToBePresentInDom(By.XPath("//*[@id = 'WorkspaceGridPopup1_DXMainTable']/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td[2]"));
            var architectures = By.XPath("//*[@id = 'WorkspaceGridPopup1_DXMainTable']/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td[2]");
            Click(By.XPath("(//*[@id = 'WorkspaceGridPopup1_DXMainTable']/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/img)[" 
                + GetElementNumberInElementsArray2(architectures, architectureName) + "]"));
            Click(By.XPath("//*[@id = 'WorkspaceGridPopup1_DXMainTable']/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/a"));          
        }
    }
}
