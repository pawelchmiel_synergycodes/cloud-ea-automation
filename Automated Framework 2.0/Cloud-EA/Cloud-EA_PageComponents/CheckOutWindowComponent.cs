﻿using Automated_Framework_2._0.Pages;
using OpenQA.Selenium;

namespace Automated_Framework_2._0
{
    public class CheckOutWindowComponent : CloudEaPage
    {
        private static readonly By ViewTemplateButton = By.Id("ViewTemplateBtn_CD");
        private static readonly By CheckOutButton = By.Id("CheckOutBtn_CD");
        private static readonly By ViewOnlyButton = By.Id("ViewOnlyBtn_CD");
        private static readonly By CheckOutLight = By.Id("checkOutLight");
        private static readonly By CheckOutWindow = By.Id("CheckOutOpenPopup_PW-1");
        public static void ClickCheckOutButton()
        {
            Click(CheckOutButton);
        }

        public static void ClickViewTemplateButton()
        {
            Click(ViewTemplateButton);
        }

        public static bool CheckIfViewTemplateButtonIsVisible()
        {
            WaitForElementToBeVisible(CheckOutWindow);
            return FindElement(ViewTemplateButton).Displayed;
        }

        public static void ClickViewOnlyButton()
        {
            Click(ViewOnlyButton);
        }

        public static bool GetCheckOutStatus() // return true when viewpoint is Checked Out
        {
            return FindElement(CheckOutLight).GetAttribute("class").Equals("circle-yellow-adm");
        }
    }
}
