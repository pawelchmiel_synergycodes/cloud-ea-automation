﻿using System;
using System.Threading;
using Automated_Framework_2._0.Pages;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;

namespace Automated_Framework_2._0
{
    public class MatricesGridViewComponent : CloudEaPage
    {
        private static readonly By AddColumnButton = By.Id("AddColBtnLogical_CD");
        private static readonly By AddRowButton = By.Id("AddRowBtnLogical_CD");

        private static void SwitchToManageTab()
        {
            if (FindElement(By.XPath("//*[@id = 'ToolSetTab_T1T']/span")).Displayed)
                Click(By.XPath("//*[@id = 'ToolSetTab_T1T']/span"));
        }

        public static void AddNewColumn(string text)
        {
            SwitchToManageTab();
            Click(AddColumnButton);
            TypeInto(By.Id("EntityNameColTBOX_I"), text);
            Click(By.XPath("(//*[@id = 'addColumnPopup_PWF-1']/div/button)[1]"));            
        }

        public static void AddNewRow(string text)
        {
            SwitchToManageTab();
            Click(AddRowButton);
            TypeInto(By.Id("EntityNameRowTBOX_I"), text);
            Click(By.XPath("(//*[@id = 'addRowPopup_PWF-1']/div/button)[1]"));
        }

        /// <summary>
        /// Navigate to deisred tab of gridview.
        /// </summary>
        /// <param name="tab">Number of desired tab in gridview(count starts from 0).</param>
        public static void SwitchToDesiredTab(int tab)
        {
            if (tab < 0) throw new ArgumentOutOfRangeException("tab");
            Click(By.Id("pageControl_T" + tab + "T"));
        }

        public static void RemoveColumnWithName(string name)
        {
            SwitchToManageTab();
            var action = new Actions(GetDriver());
            var columns = By.XPath("//*[@id = 'pivotGrid1_CVSCell_SCDTable_CG']/following-sibling::tbody/tr/td");
            var column = By.XPath("(//*[@id = 'pivotGrid1_CVSCell_SCDTable_CG']/following-sibling::tbody/tr/td)[" + GetElementNumberInElementsArray(columns, name) + "]");
            action.MoveToElement(FindElement(column), 5, 5).Build().Perform();
            var deleteColumnButton = By.XPath("(//*[@id = 'pivotGrid1_CVSCell_SCDTable_CG']/following-sibling::tbody/tr/td/img)[" + GetElementNumberInElementsArray(columns, name) + "]");
            Click(deleteColumnButton);
        }

        public static void RemoveRowWithName(string name)
        {
            SwitchToManageTab();
            var action = new Actions(GetDriver());
            var rows = By.XPath("//*[@id = 'pivotGrid1_RVSCell_SCDTable_CG']/following-sibling::tbody/tr/td");
            var row = By.XPath("(//*[@id = 'pivotGrid1_RVSCell_SCDTable_CG']/following-sibling::tbody/tr/td)[" + GetElementNumberInElementsArray(rows, name) + "]");
            action.MoveToElement(FindElement(row), 5, 5).Build().Perform();
            var deleteColumnButton = By.XPath("(//*[@id = 'pivotGrid1_RVSCell_SCDTable_CG']/following-sibling::tbody/tr/td/img)[" + GetElementNumberInElementsArray(rows, name) + "]");
            Click(deleteColumnButton);
        }

        public static void DeleteRelation(string name)
        {
            SwitchToManageTab();
            var cells = By.XPath("//*[@id = 'pivotGrid1_DCSCell_SCDTable_CG']/following-sibling::tbody/tr/td");
            var cell = By.XPath("(//*[@id = 'pivotGrid1_DCSCell_SCDTable_CG']/following-sibling::tbody/tr/td)[" + GetElementNumberInElementsArray(cells, name) + "]");
            Click(cell);
            Click(By.XPath("//*[@id = 'drillDownGridView_DXMainTable']/tbody/tr/td/img"));
            Thread.Sleep(300);
            GetDriver().SwitchTo().ActiveElement().SendKeys(Keys.Backspace);
            Click(By.Id("drillDownGridView_DXCBtn0"));
        }
    }
}
