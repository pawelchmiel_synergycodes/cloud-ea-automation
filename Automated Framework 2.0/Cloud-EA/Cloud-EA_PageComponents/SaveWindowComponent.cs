﻿using Automated_Framework_2._0.Pages;
using OpenQA.Selenium;

namespace Automated_Framework_2._0
{
    public class SaveWindowComponent : CloudEaPage
    {
        private static readonly By SaveButtonOnRibbon = By.XPath("//*[@id = 'SaveDiagramButton2']/img");
        private static readonly By SaveButton = By.XPath("//*[@id = 'SaveAndCheckInBtn']/following-sibling::button");
        private static readonly By SaveAndCheckInButton = By.Id("SaveAndCheckInBtn");
        private static readonly By OkButtonOnResultWindow = By.Id("SavingInfoCloseButton");

        public static void SaveWithoutName()
        {
            Click(SaveButtonOnRibbon);
            Click(SaveButton);
            Click(OkButtonOnResultWindow);
        }

        public static void SaveAndCheckInWithoutName()
        {
            Click(SaveButtonOnRibbon);
            Click(SaveAndCheckInButton);
            Click(OkButtonOnResultWindow);
        }

        public static void SaveAndCheckIn(string projectName)
        {
            Click(By.Id("SaveBtn"));
            WaitForElementToBeVisible(By.Id("DiagramNameTBox"));
            TypeInto(By.Id("DiagramNameTBox"), projectName);
            Click(SaveAndCheckInButton);
            Click(OkButtonOnResultWindow);
        }

        public static void Save(string projectName)
        {
            Click(By.Id("SaveBtn"));
            TypeInto(By.Id("DiagramNameTBox"), projectName);
            Click(SaveButton);
            Click(OkButtonOnResultWindow);
        }
    }
}
