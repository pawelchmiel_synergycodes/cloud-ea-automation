﻿using Automated_Framework_2._0.Utility;
using OpenQA.Selenium;
using System.Linq;
using System.Threading;

namespace Automated_Framework_2._0.Pages
{
    public class DashboardPage : CloudEaPage
    {

        public static void NavigateToApp(App app)
        {
            if (app.AppFramework != "DoDAF")
                Click(By.Id(app.AppFramework));
            WaitForElementToBeVisible(By.ClassName("dashboard-wrapper"));
            FindElements(By.XPath("//*[@data-clickable = 'true']"))[app.AppLeftMenuIndexOnDashboard].Click();
            FindElements(By.ClassName("dashboard-app-wrapper"))[app.AppMainMenuIndexOnDashboard].Click();
            // sometimes new tab won't open before we try to switch to this tab
            Thread.Sleep(100);
            GetDriver().SwitchTo().Window(GetDriver().WindowHandles.Last());
            WaitForSiteLoader();
        }
    }
}
