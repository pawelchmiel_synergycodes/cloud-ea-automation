﻿using Automated_Framework_2._0.Pages;
using OpenQA.Selenium;
using System.Configuration;

namespace Automated_Framework_2._0
{
    public class LoginPage : CloudEaPage
    {
        private static readonly By UserName = By.Id("UserName_I");
        private static readonly By UserPassword = By.Id("Password_I");
        private static readonly By LoginButton = By.XPath("//*[@type = 'submit']");

        public static void NavigateToLoginPage()
        {
            //Open("http://spdevelop.azurewebsites.net");
            Open(ConfigurationManager.AppSettings["CloudEaUrl"]);  
        }

        public static void Login(string userName, string userPassword)
        {
            TypeInto(UserName, userName);
            TypeInto(UserPassword, userPassword);
            Click(LoginButton);
            WaitForSiteLoader();
        }
    }
}
