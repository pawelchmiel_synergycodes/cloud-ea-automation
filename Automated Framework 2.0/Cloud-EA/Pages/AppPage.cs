﻿using Automated_Framework_2._0.Utility;
using OpenQA.Selenium;
using System.Drawing;
using System.Threading;

namespace Automated_Framework_2._0.Pages
{
    public class AppPage : CloudEaPage
    {
        public static bool IsAtRightApp(App app)
        {
            if (app.AppProjectNameId == "Scheduling")
                return FindElement(By.XPath("//*[@id = 'SchedulingDiv-p-0']//h2")).Text == app.AppName;
            if (app.AppProjectNameId == null)
                return GetPageTitle() == app.AppName;
            return GetPageTitle() == app.AppName || FindElement(By.Id(app.AppProjectNameId)).Text == app.AppName;
        }
    }
}
