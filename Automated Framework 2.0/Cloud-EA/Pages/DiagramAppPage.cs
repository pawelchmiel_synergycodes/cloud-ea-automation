﻿using Automated_Framework_2._0.Pages;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Configuration;

namespace Automated_Framework_2._0.Cloud_EA.Pages
{
    public class DiagramAppPage : AppPage
    {
        protected static string _userName = ConfigurationManager.AppSettings["UserName"];
        protected static string _userPassword = ConfigurationManager.AppSettings["UserPassword"];

        protected static readonly By PaletteDiv = By.Id("myPalette_DODAFViewpoints");
        protected static readonly By DiagramDiv = By.Id("myDiagram");


        public static void LogOut()
        {
            Thread.Sleep(1000);
            System.Collections.ObjectModel.ReadOnlyCollection<IWebElement> findElements = CloudEaPage.FindElements(By.ClassName("dropdown-menu"));
            findElements[1].Click();
            CloudEaPage.Click(By.LinkText("Log out"));
            CloudEaPage.WaitForSiteLoader();
        }

        /// <summary>
        /// Return offset coord relative to canvas
        /// </summary>
        /// <param name="screenCoord">Actual screen coord</param>
        /// <returns></returns>
        public static Point CanvasOffset(Point screenCoord)
        {
            Point canvasUpperLeft = new Point(425, 212);
            return new Point(screenCoord.X - canvasUpperLeft.X, screenCoord.Y - canvasUpperLeft.Y);
        }

        public static void ConnectToArchitecture(string workspaceName, string architectureName)
        {
            RepositoryWindowComponent.ConnectToArchitecture(workspaceName, architectureName);
        }

        public static void SaveToRepository(string projectName)
        {
            SaveWindowComponent.Save(projectName);
        }

        public static void ClickNewButton()
        {
            Click(By.Id("NewBtn"));
            Click(By.Id("diagramConfirmationPopupNo"));
            GetDriver().SwitchTo().Alert().Accept();
            WaitForSiteLoader();
        }

        public static void CheckOutDiagram(string projectName)
        {
            Click(By.Id("OpenBtn"));
            WaitForElementToBePresentInDom(By.Id("OpenMasterGrid_DXMainTable"));
            int number = GetElementNumberInElementsArray2(By.XPath("//*[@id = 'OpenMasterGrid_DXMainTable']/tbody/tr[1]/td[1]"), projectName);
            //Click(By.XPath("//*[@id = 'OpenMasterGrid_DXMainTable']/tbody/tr[" + number + "]/td[4]/span[1]"));
            Click(By.XPath("//*[@id = 'OpenMasterGrid_DXDataRow0']/td[5]/span[2]"));
        }

        public static void DeleteDiagram(string projectName)
        {
            Click(By.Id("OpenBtn"));
            while (true)
            {             
                WaitForElementToBePresentInDom(By.Id("OpenMasterGrid_DXMainTable"));
                //int number = GetElementNumberInElementsArray2(By.XPath("//*[@id = 'OpenMasterGrid_DXMainTable']/tbody/tr/td"), projectName);
                //Click(By.XPath("//*[@id = 'OpenMasterGrid_DXMainTable']/tbody/tr[" + number + "]/td[5]/span[2]"));
                Click(By.XPath("//*[@id = 'OpenMasterGrid_DXDataRow0']/td[5]/span[3]"));
                WaitForElementToBePresentInDom(By.Id("diagramConfirmationPopup_PWC-1"));
                //Thread.Sleep(1000);
                Click(By.Id("diagramConfirmationPopupYes"));
                break;
            }
        }

        public static void SaveAndCheckInToRepository(string projectName)
        {
            SaveWindowComponent.SaveAndCheckIn(projectName);
            //WaitForSiteLoader();
            //IWebElement findElement = FindElement(By.Id("DiagramStillSavingPopup_PWC-1"));
            //while (findElement != null)
            //{
                
            //}
        }
    }
}
