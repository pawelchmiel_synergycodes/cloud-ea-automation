﻿using System;
using System.Collections.ObjectModel;
using System.Threading;
using Automated_Framework_2._0.Utility;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace Automated_Framework_2._0.Pages
{
    public class CloudEaPage : AbstractPage
    {
        private static WebDriverWait _wait;

        public new static string GetPageSource()
        {
            WaitForSiteLoader();
            return GetDriver().PageSource;
        }

        public new static IWebElement FindElement(By locator)
        {
            WaitForSiteLoader();
            return GetDriver().FindElement(locator);
        }

        public new static ReadOnlyCollection<IWebElement> FindElements(By locator)
        {
            WaitForSiteLoader();
            return GetDriver().FindElements(locator);
        }

        public new static void TypeInto(By locator, string text)
        {
            FindElement(locator).Clear();
            FindElement(locator).SendKeys(text);
        }

        public new static void Click(By locator)
        {
            WaitForSiteLoader();
            _wait = new WebDriverWait(GetDriver(), TimeSpan.FromSeconds(30));
            _wait.Until(ExpectedConditions.ElementToBeClickable(locator));
            FindElement(locator).Click();
        }

        public static void WaitForSiteLoader()
        {
            _wait = new WebDriverWait(GetDriver(), TimeSpan.FromSeconds(60));
            _wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.ClassName("loader")));
        }

        public static void WaitForComponentReadyState(string propertyName)
        {
            int counter = 0;
            string x;
            var js = GetDriver() as IJavaScriptExecutor;
            if (js == null) return;
            do
            {
                Thread.Sleep(10);
                var active = js.ExecuteScript(@"if("+ propertyName + "){ return " + propertyName + " } return false;");
                x = active.ToString();
                counter++;
                if (counter >= 3000)
                    break;
            } while (x == "False");
        }

        public static void WaitForComponentInCallback(string functionName)
        {
            int counter = 0;
            string x;
            var js = GetDriver() as IJavaScriptExecutor;
            if (js == null) return;
            do
            {
                Thread.Sleep(10);
                var active = js.ExecuteScript(@"if(" + functionName + "){ return " + functionName + " } return false;");
                x = active.ToString();
                counter++;
                if (counter >= 3000)
                    break;
            } while (x == "True");
        }

        public static bool CheckForDataInDataBase(string app, string viewpoint)
        {
            var js = GetDriver() as IJavaScriptExecutor;
            if (js == null) return false;
            var statuses = js.ExecuteScript(@" return $.ajax({ 
	            type: ""GET"",
	            url: ""/DiagramTogaf/GetOpenCheckOutInfo"",
	                async: false,
	                data: { app: ""{0}"", template: ""{1}"", isGeneric: false }
                }).responseText;".Replace("{0}", app).Replace("{1}", viewpoint));
            return ((string) statuses).Contains("true");
        }
    }
}
