﻿using Automated_Framework_2._0.Cloud_EA.Cloud_EA_PageComponents;
using Automated_Framework_2._0.Cloud_EA.Pages;
using Automated_Framework_2._0.Pages;
using Automated_Framework_2._0.Utility;
using Automated_Framework_2._0.Utility.Enums;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Automated_Framework_2._0.Cloud_EA.DoDAF.Diagram
{
    public class CV2Page : DiagramAppPage
    {
        public static void LoadApp()
        {
            LoginPage.NavigateToLoginPage();
            LoginPage.Login(_userName, _userPassword);

            DashboardPage.NavigateToApp(new App(AppsNames.DodafCv2));
        }

        public static void TestC61(bool deleteProjectAfterTest)
        {
            LoadApp();
            RunC61Scenario(deleteProjectAfterTest);
        }

        private static void RunC61Scenario(bool deleteProjectAfterTest)
        {
            Point capabilityPalette = new Point(198, 31);

            ConnectToArchitecture("__AutoTest", "DoDAF");

            // Step 1
            //
            CanvasInteractions.DragAndDropFromPaletteToDiagram(PaletteDiv, DiagramDiv, capabilityPalette, new Point(685, 138)); // CanvasOffset(new Point(1110, 350)));

            // Step 2
            //
            CanvasInteractions.DragAndDropFromPaletteToDiagram(PaletteDiv, DiagramDiv, capabilityPalette, new Point(685, 168)); // CanvasOffset(new Point(1110, 380)));
            CanvasInteractions.DragAndDropFromPaletteToDiagram(PaletteDiv, DiagramDiv, capabilityPalette, new Point(685, 168)); // CanvasOffset(new Point(1110, 380)));

            // Step 3
            //
            CanvasInteractions.DragAndDropFromPaletteToDiagram(PaletteDiv, DiagramDiv, capabilityPalette, new Point(610, 318)); // CanvasOffset(new Point(1035, 530)));
            CanvasInteractions.DragAndDropFromPaletteToDiagram(PaletteDiv, DiagramDiv, capabilityPalette, new Point(610, 318)); // CanvasOffset(new Point(1035, 530)));

            // Step 4
            //
            CanvasInteractions.DragAndDropFromPaletteToDiagram(PaletteDiv, DiagramDiv, capabilityPalette, new Point(763, 318)); // CanvasOffset(new Point(1188, 530)));
            CanvasInteractions.DragAndDropFromPaletteToDiagram(PaletteDiv, DiagramDiv, capabilityPalette, new Point(763, 318)); // CanvasOffset(new Point(1188, 530)));

            Thread.Sleep(3000);

            // Step 8
            //
            SaveAndCheckInToRepository("CV-2");
            Thread.Sleep(60000);

            // Step 9 & 10
            //
            //CheckOutDiagram("CV-2");


            //SaveAndCheckInToRepository("CV-2");

            if (deleteProjectAfterTest)
                DeleteDiagram("CV-2");

        }

        private static Point PaletteOffset(Point screenCoord)
        {
            Point palleteUpperLeft = new Point(6, 317);
            return new Point(screenCoord.X - palleteUpperLeft.X, screenCoord.Y - palleteUpperLeft.Y);
        }
    }
}
