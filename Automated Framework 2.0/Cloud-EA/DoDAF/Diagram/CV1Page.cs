﻿using Automated_Framework_2._0.Cloud_EA.Cloud_EA_PageComponents;
using Automated_Framework_2._0.Cloud_EA.Pages;
using Automated_Framework_2._0.Pages;
using Automated_Framework_2._0.Utility;
using Automated_Framework_2._0.Utility.Enums;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Automated_Framework_2._0.Cloud_EA.DoDAF.Diagram
{
    public class CV1Page : DiagramAppPage
    {
        public static void LoadApp()
        {
            LoginPage.NavigateToLoginPage();
            LoginPage.Login(_userName, _userPassword);

            DashboardPage.NavigateToApp(new App(AppsNames.DodafCv1));
        }

        public static void TestC59(bool deleteProjectAfterTest)
        {
            LoadApp();
            RunC59Scenario(deleteProjectAfterTest);
        }

        private static void RunC59Scenario(bool deleteProjectAfterTest)
        {
            Point VisionPalette = PaletteOffset(new Point(100, 348));
            Point GoalPalette = PaletteOffset(new Point(204, 348));
            Point CapabilityPalette = PaletteOffset(new Point(310, 348));

            ConnectToArchitecture("__AutoTest", "DoDAF");

            // Step 1
            //
            CanvasInteractions.DragAndDropFromPaletteToDiagram(PaletteDiv, DiagramDiv, VisionPalette, CanvasOffset(new Point(1110, 350)));
            
            // Step 2
            //
            CanvasInteractions.DragAndDropFromPaletteToDiagram(PaletteDiv, DiagramDiv, GoalPalette, CanvasOffset(new Point(1110, 380)));
            
            // Step 3
            //
            CanvasInteractions.DragAndDropFromPaletteToDiagram(PaletteDiv, DiagramDiv, GoalPalette, CanvasOffset(new Point(1110, 380)));

            // Step 4
            //
            CanvasInteractions.DragAndDropFromPaletteToDiagram(PaletteDiv, DiagramDiv, CapabilityPalette, CanvasOffset(new Point(1035, 530)));

            // Step 5
            //
            CanvasInteractions.DragAndDropFromPaletteToDiagram(PaletteDiv, DiagramDiv, CapabilityPalette, CanvasOffset(new Point(1035, 530)));

            // Step 6
            //
            CanvasInteractions.DragAndDropFromPaletteToDiagram(PaletteDiv, DiagramDiv, CapabilityPalette, CanvasOffset(new Point(1188, 530)));

            // Step 7
            //
            CanvasInteractions.DragAndDropFromPaletteToDiagram(PaletteDiv, DiagramDiv, CapabilityPalette, CanvasOffset(new Point(1188, 530)));

            // Rename nodes
            //
            CanvasInteractions.RenameNode("V1", DiagramDiv, CanvasOffset(new Point(1058, 283)));
            CanvasInteractions.RenameNode("G1", DiagramDiv, CanvasOffset(new Point(995, 469)));
            CanvasInteractions.RenameNode("G2", DiagramDiv, CanvasOffset(new Point(1140, 469)));
            CanvasInteractions.RenameNode("C1.1", DiagramDiv, CanvasOffset(new Point(1080, 656)));
            CanvasInteractions.RenameNode("C1.2", DiagramDiv, CanvasOffset(new Point(1080, 818)));
            CanvasInteractions.RenameNode("C2.1", DiagramDiv, CanvasOffset(new Point(1230, 656)));
            CanvasInteractions.RenameNode("C2.2", DiagramDiv, CanvasOffset(new Point(1230, 818)));

            Thread.Sleep(3000);

            // Step 8
            //
            SaveAndCheckInToRepository("CV-1");
            Thread.Sleep(60000);

            // Step 9 & 10
            //
            //CheckOutDiagram("CV-1");


            //SaveAndCheckInToRepository("CV-1");

            if (deleteProjectAfterTest)
                DeleteDiagram("CV-1");

        }

        private static Point PaletteOffset(Point screenCoord)
        {
            Point palleteUpperLeft = new Point(6, 317);
            return new Point(screenCoord.X - palleteUpperLeft.X, screenCoord.Y - palleteUpperLeft.Y);
        }
    }
}
