﻿using Automated_Framework_2._0.Cloud_EA.Cloud_EA_PageComponents;
using Automated_Framework_2._0.Cloud_EA.Pages;
using Automated_Framework_2._0.Pages;
using Automated_Framework_2._0.Utility;
using Automated_Framework_2._0.Utility.Enums;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Automated_Framework_2._0.Cloud_EA.DoDAF.Diagram
{
    public class CV4Page : DiagramAppPage
    {
        public static void LoadApp()
        {
            LoginPage.NavigateToLoginPage();
            LoginPage.Login(_userName, _userPassword);

            DashboardPage.NavigateToApp(new App(AppsNames.DodafCv4));
        }

        public static void TestC63(bool deleteProjectAfterTest)
        {
            LoadApp();
            RunC63Scenario(deleteProjectAfterTest);
        }

        private static void RunC63Scenario(bool deleteProjectAfterTest)
        {
            Point capabilityPalette = PaletteOffset(new Point(205, 348));

            ConnectToArchitecture("__AutoTest", "DoDAF");

            // Step 1
            //
            CanvasInteractions.DragAndDropFromPaletteToDiagram(PaletteDiv, DiagramDiv, capabilityPalette, CanvasOffset(new Point(837, 418)));
            
            // Step 2
            //
            CanvasInteractions.DragAndDropFromPaletteToDiagram(PaletteDiv, DiagramDiv, capabilityPalette, CanvasOffset(new Point(1167, 421)));
            
            // Step 3
            //
            CanvasInteractions.DragAndDropFromPaletteToDiagram(PaletteDiv, DiagramDiv, capabilityPalette, CanvasOffset(new Point(995, 585)));

            // Step 4
            //
            CanvasInteractions.ClickAndDragOnCanvas(DiagramDiv, CanvasOffset(new Point(897, 423)), CanvasOffset(new Point(1125, 424)));

            // Step 5
            //
            CanvasInteractions.ClickAndDragOnCanvas(DiagramDiv, CanvasOffset(new Point(865, 434)), CanvasOffset(new Point(973, 593)));

            // Step 6
            //
            CanvasInteractions.ClickAndDragOnCanvas(DiagramDiv, CanvasOffset(new Point(1125, 434)), CanvasOffset(new Point(973, 593)));

            Thread.Sleep(3000);

            // Step 7
            //
            SaveAndCheckInToRepository("CV-4");
            Thread.Sleep(60000);

            //// Step 9 & 10
            ////
            ////CheckOutDiagram("CV-4");


            ////SaveAndCheckInToRepository("CV-4");

            if (deleteProjectAfterTest)
                DeleteDiagram("CV-4");

        }

        private static Point PaletteOffset(Point screenCoord)
        {
            Point palleteUpperLeft = new Point(6, 317);
            return new Point(screenCoord.X - palleteUpperLeft.X, screenCoord.Y - palleteUpperLeft.Y);
        }
    }
}
