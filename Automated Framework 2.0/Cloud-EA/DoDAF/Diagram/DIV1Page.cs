﻿using Automated_Framework_2._0.Cloud_EA.Cloud_EA_PageComponents;
using Automated_Framework_2._0.Cloud_EA.Pages;
using Automated_Framework_2._0.Pages;
using Automated_Framework_2._0.Utility;
using Automated_Framework_2._0.Utility.Enums;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Automated_Framework_2._0.Cloud_EA.DoDAF.Diagram
{
    public class DIV1Page : DiagramAppPage
    {
        public static void LoadApp()
        {
            LoginPage.NavigateToLoginPage();
            LoginPage.Login(_userName, _userPassword);

            DashboardPage.NavigateToApp(new App(AppsNames.DodafDiv1));
        }

        public static void TestC88(bool deleteProjectAfterTest)
        {
            LoadApp();
            RunC88Scenario(deleteProjectAfterTest);
        }

        private static void RunC88Scenario(bool deleteProjectAfterTest)
        {
            Point divPalette = PaletteOffset(new Point(205, 348));

            ConnectToArchitecture("__AutoTest", "DoDAF");

            // Step 1
            //
            CanvasInteractions.DragAndDropFromPaletteToDiagram(PaletteDiv, DiagramDiv, divPalette, CanvasOffset(new Point(730, 350)));
            CanvasInteractions.DragAndDropFromPaletteToDiagram(PaletteDiv, DiagramDiv, divPalette, CanvasOffset(new Point(1050, 350)));
            CanvasInteractions.DragAndDropFromPaletteToDiagram(PaletteDiv, DiagramDiv, divPalette, CanvasOffset(new Point(880, 540)));

            // Step 2
            //
            CanvasInteractions.ClickAndDragOnCanvas(DiagramDiv, CanvasOffset(new Point(762, 360)), CanvasOffset(new Point(1008, 360)));
            CanvasInteractions.ClickOnCanvas(DiagramDiv, CanvasOffset(new Point(1020, 355)));

            // Step 3
            //
            CanvasInteractions.ClickAndDragOnCanvas(DiagramDiv, CanvasOffset(new Point(1036, 369)), CanvasOffset(new Point(893, 528)));
            CanvasInteractions.ClickOnCanvas(DiagramDiv, CanvasOffset(new Point(1062, 481)));

            // Step 4
            //
            CanvasInteractions.ClickAndDragOnCanvas(DiagramDiv, CanvasOffset(new Point(892, 528)), CanvasOffset(new Point(726, 368)));
            CanvasInteractions.ClickOnCanvas(DiagramDiv, CanvasOffset(new Point(890, 509)));

            //Thread.Sleep(3000);

            // Step 5
            //
            SaveAndCheckInToRepository("DIV-1");

            Thread.Sleep(60000);

            if (deleteProjectAfterTest)
                DeleteDiagram("DIV-1");

        }

        private static Point PaletteOffset(Point screenCoord)
        {
            Point palleteUpperLeft = new Point(6, 317);
            return new Point(screenCoord.X - palleteUpperLeft.X, screenCoord.Y - palleteUpperLeft.Y);
        }
    }
}
