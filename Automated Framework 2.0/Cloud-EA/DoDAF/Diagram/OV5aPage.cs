﻿using Automated_Framework_2._0.Cloud_EA.Cloud_EA_PageComponents;
using Automated_Framework_2._0.Cloud_EA.Pages;
using Automated_Framework_2._0.Pages;
using Automated_Framework_2._0.Utility;
using Automated_Framework_2._0.Utility.Enums;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Automated_Framework_2._0.Cloud_EA.DoDAF.Diagram
{
    public class OV5aPage : DiagramAppPage
    {
        public static void LoadApp()
        {
            LoginPage.NavigateToLoginPage();
            LoginPage.Login(_userName, _userPassword);

            DashboardPage.NavigateToApp(new App(AppsNames.DodafOv5a));
        }

        public static void TestC70(bool deleteProjectAfterTest)
        {
            LoadApp();
            RunC70Scenario(deleteProjectAfterTest);
        }

        private static void RunC70Scenario(bool deleteProjectAfterTest)
        {
            Point divPalette = PaletteOffset(new Point(205, 348));

            ConnectToArchitecture("__AutoTest", "DoDAF");

            // Step 5
            //
            SaveAndCheckInToRepository("OV-5a");

            Thread.Sleep(60000);

            if (deleteProjectAfterTest)
                DeleteDiagram("OV-5a");

        }

        private static Point PaletteOffset(Point screenCoord)
        {
            Point palleteUpperLeft = new Point(6, 317);
            return new Point(screenCoord.X - palleteUpperLeft.X, screenCoord.Y - palleteUpperLeft.Y);
        }

        public static void TestC157(bool deleteProjectAfterTest)
        {
            LoadApp();
            RunC157Scenario(deleteProjectAfterTest);
        }

        private static void RunC157Scenario(bool deleteProjectAfterTest)
        {
            Point divPalette = PaletteOffset(new Point(205, 348));

            ConnectToArchitecture("__AutoTest", "DoDAF");

            // Step 5
            //
            SaveAndCheckInToRepository("OV-5a");

            Thread.Sleep(60000);

            if (deleteProjectAfterTest)
                DeleteDiagram("OV-5a");
        }
    }
}
