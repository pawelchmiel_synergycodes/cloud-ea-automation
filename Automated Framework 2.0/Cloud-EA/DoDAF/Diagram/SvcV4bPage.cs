﻿using Automated_Framework_2._0.Cloud_EA.Pages;
using Automated_Framework_2._0.Pages;
using Automated_Framework_2._0.Utility;
using Automated_Framework_2._0.Utility.Enums;
using System.Drawing;
using System.Threading;

namespace Automated_Framework_2._0.Cloud_EA.DoDAF.Diagram
{
    public class SvcV4bPage : DiagramAppPage
    {
        public static void LoadApp()
        {
            LoginPage.NavigateToLoginPage();
            LoginPage.Login(_userName, _userPassword);

            DashboardPage.NavigateToApp(new App(AppsNames.DodafSvcv4b));
        }

        public static void TestC181(bool deleteProjectAfterTest)
        {
            LoadApp();
            RunC181Scenario(deleteProjectAfterTest);
        }

        private static void RunC181Scenario(bool deleteProjectAfterTest)
        {
            Point divPalette = PaletteOffset(new Point(205, 348));

            ConnectToArchitecture("__AutoTest", "DoDAF");

            // Step 5
            //
            SaveAndCheckInToRepository("SvcV-4b");

            Thread.Sleep(60000);

            if (deleteProjectAfterTest)
                DeleteDiagram("SvcV-4b");

        }

        private static Point PaletteOffset(Point screenCoord)
        {
            Point palleteUpperLeft = new Point(6, 317);
            return new Point(screenCoord.X - palleteUpperLeft.X, screenCoord.Y - palleteUpperLeft.Y);
        }
    }
}
