﻿using Automated_Framework_2._0.Cloud_EA.Cloud_EA_PageComponents;
using Automated_Framework_2._0.Cloud_EA.Pages;
using Automated_Framework_2._0.Pages;
using Automated_Framework_2._0.Utility;
using Automated_Framework_2._0.Utility.Enums;
using System.Drawing;
using System.Threading;

namespace Automated_Framework_2._0.Cloud_EA.DoDAF.Diagram
{
    public class OV4Page : DiagramAppPage
    {
        public static void LoadApp()
        {
            LoginPage.NavigateToLoginPage();
            LoginPage.Login(_userName, _userPassword);

            DashboardPage.NavigateToApp(new App(AppsNames.DodafOv4));
        }

        public static void TestC174(bool deleteProjectAfterTest)
        {
            LoadApp();
            RunC174Scenario(deleteProjectAfterTest);
        }

        private static void RunC174Scenario(bool deleteProjectAfterTest)
        {
            Point orgPalette = PaletteOffset(new Point(205, 348));

            ConnectToArchitecture("__AutoTest", "DoDAF");

            // Step 5
            //
            SaveAndCheckInToRepository("OV-4");

            Thread.Sleep(60000);

            if (deleteProjectAfterTest)
                DeleteDiagram("OV-4");

        }

        public static void TestC71(bool deleteProjectAfterTest)
        {
            LoadApp();
            RunC71Scenario(deleteProjectAfterTest);
        }

        private static void RunC71Scenario(bool deleteProjectAfterTest)
        {
            Point orgPalette = PaletteOffset(new Point(205, 348));

            ConnectToArchitecture("__AutoTest", "DoDAF");

            // Step 1
            //
            CanvasInteractions.DragAndDropFromPaletteToDiagram(PaletteDiv, DiagramDiv, orgPalette, new Point(685, 138)); // CanvasOffset(new Point(1110, 350)));

            // Step 2
            //
            CanvasInteractions.DragAndDropFromPaletteToDiagram(PaletteDiv, DiagramDiv, orgPalette, new Point(685, 168)); // CanvasOffset(new Point(1110, 380)));
            CanvasInteractions.DragAndDropFromPaletteToDiagram(PaletteDiv, DiagramDiv, orgPalette, new Point(685, 168)); // CanvasOffset(new Point(1110, 380)));

            // Step 3
            //
            CanvasInteractions.DragAndDropFromPaletteToDiagram(PaletteDiv, DiagramDiv, orgPalette, new Point(610, 318)); // CanvasOffset(new Point(1035, 530)));
            CanvasInteractions.DragAndDropFromPaletteToDiagram(PaletteDiv, DiagramDiv, orgPalette, new Point(610, 318)); // CanvasOffset(new Point(1035, 530)));

            // Step 4
            //
            CanvasInteractions.DragAndDropFromPaletteToDiagram(PaletteDiv, DiagramDiv, orgPalette, new Point(763, 318)); // CanvasOffset(new Point(1188, 530)));
            CanvasInteractions.DragAndDropFromPaletteToDiagram(PaletteDiv, DiagramDiv, orgPalette, new Point(763, 318)); // CanvasOffset(new Point(1188, 530)));

            // Step 5
            //
            SaveAndCheckInToRepository("OV-4");

            Thread.Sleep(60000);

            if (deleteProjectAfterTest)
                DeleteDiagram("OV-4");

        }

        private static Point PaletteOffset(Point screenCoord)
        {
            Point palleteUpperLeft = new Point(6, 317);
            return new Point(screenCoord.X - palleteUpperLeft.X, screenCoord.Y - palleteUpperLeft.Y);
        }
    }
}
