﻿using Automated_Framework_2._0.Cloud_EA.Cloud_EA_PageComponents;
using Automated_Framework_2._0.Cloud_EA.Pages;
using Automated_Framework_2._0.Pages;
using Automated_Framework_2._0.Utility;
using Automated_Framework_2._0.Utility.Enums;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Automated_Framework_2._0.Cloud_EA.DoDAF.Diagram
{
    public class SV1Page : DiagramAppPage
    {
        public static void LoadApp()
        {
            LoginPage.NavigateToLoginPage();
            LoginPage.Login(_userName, _userPassword);

            DashboardPage.NavigateToApp(new App(AppsNames.DodafSv1));
        }

        public static void TestC137(bool deleteProjectAfterTest)
        {
            LoadApp();
            RunC137Scenario(deleteProjectAfterTest);
        }

        private static void RunC137Scenario(bool deleteProjectAfterTest)
        {
            Point divPalette = PaletteOffset(new Point(205, 348));

            ConnectToArchitecture("__AutoTest", "DoDAF");

            // Step 5
            //
            SaveAndCheckInToRepository("SV-1");

            Thread.Sleep(60000);

            if (deleteProjectAfterTest)
                DeleteDiagram("SV-1");

        }

        private static Point PaletteOffset(Point screenCoord)
        {
            Point palleteUpperLeft = new Point(6, 317);
            return new Point(screenCoord.X - palleteUpperLeft.X, screenCoord.Y - palleteUpperLeft.Y);
        }

        public static void TestC182(bool deleteProjectAfterTest)
        {
            LoadApp();
            RunC182Scenario(deleteProjectAfterTest);
        }

        private static void RunC182Scenario(bool deleteProjectAfterTest)
        {
            Point divPalette = PaletteOffset(new Point(205, 348));

            ConnectToArchitecture("__AutoTest", "DoDAF");

            // Step 5
            //
            SaveAndCheckInToRepository("SV-1");

            Thread.Sleep(60000);

            if (deleteProjectAfterTest)
                DeleteDiagram("SV-1");
        }


    }
}
