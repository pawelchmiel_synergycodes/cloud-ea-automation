﻿using Automated_Framework_2._0.Cloud_EA.Pages;
using Automated_Framework_2._0.Pages;
using Automated_Framework_2._0.Utility;
using Automated_Framework_2._0.Utility.Enums;
using System.Drawing;
using System.Threading;

namespace Automated_Framework_2._0.Cloud_EA.DoDAF.Diagram
{
    public class SV4aPage : DiagramAppPage
    {
        public static void LoadApp()
        {
            LoginPage.NavigateToLoginPage();
            LoginPage.Login(_userName, _userPassword);

            DashboardPage.NavigateToApp(new App(AppsNames.DodafSv4a));
        }

        public static void TestC185(bool deleteProjectAfterTest)
        {
            LoadApp();
            RunC185Scenario(deleteProjectAfterTest);
        }

        private static void RunC185Scenario(bool deleteProjectAfterTest)
        {
            Point divPalette = PaletteOffset(new Point(205, 348));

            ConnectToArchitecture("__AutoTest", "DoDAF");

            // Step 5
            //
            SaveAndCheckInToRepository("SV-4a");

            Thread.Sleep(60000);

            if (deleteProjectAfterTest)
                DeleteDiagram("SV-4a");

        }

        private static Point PaletteOffset(Point screenCoord)
        {
            Point palleteUpperLeft = new Point(6, 317);
            return new Point(screenCoord.X - palleteUpperLeft.X, screenCoord.Y - palleteUpperLeft.Y);
        }
    }
}
