﻿using Automated_Framework_2._0.Cloud_EA.Cloud_EA_PageComponents;
using Automated_Framework_2._0.Cloud_EA.Pages;
using Automated_Framework_2._0.Pages;
using Automated_Framework_2._0.Utility;
using Automated_Framework_2._0.Utility.Enums;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Automated_Framework_2._0.Cloud_EA.DoDAF.Diagram
{
    public class SvcV2Page : DiagramAppPage
    {
        public static void LoadApp()
        {
            LoginPage.NavigateToLoginPage();
            LoginPage.Login(_userName, _userPassword);

            DashboardPage.NavigateToApp(new App(AppsNames.DodafSvcv2));
        }

        public static void TestC135(bool deleteProjectAfterTest)
        {
            LoadApp();
            RunC135Scenario(deleteProjectAfterTest);
        }

        private static void RunC135Scenario(bool deleteProjectAfterTest)
        {
            Point divPalette = PaletteOffset(new Point(205, 348));

            ConnectToArchitecture("__AutoTest", "DoDAF");

            // Step 5
            //
            SaveAndCheckInToRepository("SvcV-2");

            Thread.Sleep(60000);

            if (deleteProjectAfterTest)
                DeleteDiagram("SvcV-2");

        }

        public static void TestC179(bool deleteProjectAfterTest)
        {
            LoadApp();
            RunC179Scenario(deleteProjectAfterTest);
        }

        private static void RunC179Scenario(bool deleteProjectAfterTest)
        {
            Point divPalette = PaletteOffset(new Point(205, 348));

            ConnectToArchitecture("__AutoTest", "DoDAF");

            // Step 5
            //
            SaveAndCheckInToRepository("SvcV-2");

            Thread.Sleep(60000);

            if (deleteProjectAfterTest)
                DeleteDiagram("SvcV-2");

        }

        private static Point PaletteOffset(Point screenCoord)
        {
            Point palleteUpperLeft = new Point(6, 317);
            return new Point(screenCoord.X - palleteUpperLeft.X, screenCoord.Y - palleteUpperLeft.Y);
        }
    }
}
