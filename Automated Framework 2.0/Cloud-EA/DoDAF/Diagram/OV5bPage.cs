﻿using Automated_Framework_2._0.Cloud_EA.Cloud_EA_PageComponents;
using Automated_Framework_2._0.Cloud_EA.Pages;
using Automated_Framework_2._0.Pages;
using Automated_Framework_2._0.Utility;
using Automated_Framework_2._0.Utility.Enums;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Automated_Framework_2._0.Cloud_EA.DoDAF.Diagram
{
    public class OV5bPage : DiagramAppPage
    {
        public static void LoadApp()
        {
            LoginPage.NavigateToLoginPage();
            LoginPage.Login(_userName, _userPassword);

            DashboardPage.NavigateToApp(new App(AppsNames.DodafOv5b));
        }

        public static void TestC97(bool deleteProjectAfterTest)
        {
            LoadApp();
            RunC97Scenario(deleteProjectAfterTest);
        }

        private static void RunC97Scenario(bool deleteProjectAfterTest)
        {
            Point divPalette = PaletteOffset(new Point(205, 348));

            ConnectToArchitecture("__AutoTest", "DoDAF");

            // Step 5
            //
            SaveAndCheckInToRepository("OV-5b");

            Thread.Sleep(60000);

            if (deleteProjectAfterTest)
                DeleteDiagram("OV-5b");

        }

        private static Point PaletteOffset(Point screenCoord)
        {
            Point palleteUpperLeft = new Point(6, 317);
            return new Point(screenCoord.X - palleteUpperLeft.X, screenCoord.Y - palleteUpperLeft.Y);
        }

        public static void TestC176(bool deleteProjectAfterTest)
        {
            LoadApp();
            RunC176Scenario(deleteProjectAfterTest);
        }

        private static void RunC176Scenario(bool deleteProjectAfterTest)
        {
            Point divPalette = PaletteOffset(new Point(205, 348));

            ConnectToArchitecture("__AutoTest", "DoDAF");

            // Step 5
            //
            SaveAndCheckInToRepository("OV-5b");

            Thread.Sleep(60000);

            if (deleteProjectAfterTest)
                DeleteDiagram("OV-5b");

        }

    }
}
