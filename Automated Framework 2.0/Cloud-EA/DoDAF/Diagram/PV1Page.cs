﻿using Automated_Framework_2._0.Cloud_EA.Cloud_EA_PageComponents;
using Automated_Framework_2._0.Cloud_EA.Pages;
using Automated_Framework_2._0.Pages;
using Automated_Framework_2._0.Utility;
using Automated_Framework_2._0.Utility.Enums;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Automated_Framework_2._0.Cloud_EA.DoDAF.Diagram
{
    public class PV1Page : DiagramAppPage
    {
        public static void LoadApp()
        {
            LoginPage.NavigateToLoginPage();
            LoginPage.Login(_userName, _userPassword);

            DashboardPage.NavigateToApp(new App(AppsNames.DodafPv1));
        }

        public static void TestC132(bool deleteProjectAfterTest)
        {
            LoadApp();
            RunC132Scenario(deleteProjectAfterTest);
        }

        private static void RunC132Scenario(bool deleteProjectAfterTest)
        {
            Point projPalette = new Point(146, 35);
            Point orgPalette = new Point(251, 35);

            ConnectToArchitecture("__AutoTest", "DoDAF");

            // Step 1
            //
            CanvasInteractions.DragAndDropFromPaletteToDiagram(PaletteDiv, DiagramDiv, orgPalette, new Point(685, 138));

            // Step 2
            //
            CanvasInteractions.DragAndDropFromPaletteToDiagram(PaletteDiv, DiagramDiv, projPalette, new Point(685, 168)); // CanvasOffset(new Point(1110, 380)));
            CanvasInteractions.DragAndDropFromPaletteToDiagram(PaletteDiv, DiagramDiv, projPalette, new Point(685, 168)); // CanvasOffset(new Point(1110, 380)));

            // Step 3
            //
            CanvasInteractions.DragAndDropFromPaletteToDiagram(PaletteDiv, DiagramDiv, projPalette, new Point(610, 318)); // CanvasOffset(new Point(1035, 530)));
            CanvasInteractions.DragAndDropFromPaletteToDiagram(PaletteDiv, DiagramDiv, projPalette, new Point(610, 318)); // CanvasOffset(new Point(1035, 530)));

            // Step 4
            //
            CanvasInteractions.DragAndDropFromPaletteToDiagram(PaletteDiv, DiagramDiv, projPalette, new Point(763, 318)); // CanvasOffset(new Point(1188, 530)));
            CanvasInteractions.DragAndDropFromPaletteToDiagram(PaletteDiv, DiagramDiv, projPalette, new Point(763, 318)); // CanvasOffset(new Point(1188, 530)));

            // Rename nodes
            //
            CanvasInteractions.RenameNode("O1", DiagramDiv, new Point(633, 71));  //CanvasOffset(new Point(1058, 283)));
            CanvasInteractions.RenameNode("P1", DiagramDiv, new Point(570, 257)); //CanvasOffset(new Point(995, 469)));
            CanvasInteractions.RenameNode("P2", DiagramDiv, new Point(715, 257)); //CanvasOffset(new Point(1140, 469)));
            CanvasInteractions.RenameNode("P1.1", DiagramDiv, new Point(655, 444)); //CanvasOffset(new Point(1080, 656)));
            CanvasInteractions.RenameNode("P1.2", DiagramDiv, new Point(655, 606)); //CanvasOffset(new Point(1080, 818)));
            CanvasInteractions.RenameNode("P2.1", DiagramDiv, new Point(805, 444)); //CanvasOffset(new Point(1230, 656)));
            CanvasInteractions.RenameNode("P2.2", DiagramDiv, new Point(805, 606)); //CanvasOffset(new Point(1230, 818)));

            // Step 5
            //
            SaveAndCheckInToRepository("PV-1");

            Thread.Sleep(60000);

            if (deleteProjectAfterTest)
                DeleteDiagram("PV-1");

        }

        public static void TestC177(bool deleteProjectAfterTest)
        {
            LoadApp();
            RunC177Scenario(deleteProjectAfterTest);
        }

        private static void RunC177Scenario(bool deleteProjectAfterTest)
        {
            ConnectToArchitecture("__AutoTest", "DoDAF");

            // Step 5
            //
            SaveAndCheckInToRepository("PV-1");

            Thread.Sleep(60000);

            if (deleteProjectAfterTest)
                DeleteDiagram("PV-1");

        }

        private static Point PaletteOffset(Point screenCoord)
        {
            Point palleteUpperLeft = new Point(6, 317);
            return new Point(screenCoord.X - palleteUpperLeft.X, screenCoord.Y - palleteUpperLeft.Y);
        }
    }
}
