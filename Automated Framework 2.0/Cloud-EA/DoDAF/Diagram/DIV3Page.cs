﻿using Automated_Framework_2._0.Cloud_EA.Cloud_EA_PageComponents;
using Automated_Framework_2._0.Cloud_EA.Pages;
using Automated_Framework_2._0.Pages;
using Automated_Framework_2._0.Utility;
using Automated_Framework_2._0.Utility.Enums;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Automated_Framework_2._0.Cloud_EA.DoDAF.Diagram
{
    public class DIV3Page : DiagramAppPage
    {
        public static void LoadApp()
        {
            LoginPage.NavigateToLoginPage();
            LoginPage.Login(_userName, _userPassword);

            DashboardPage.NavigateToApp(new App(AppsNames.DodafDiv3));
        }

        public static void TestC94(bool deleteProjectAfterTest)
        {
            LoadApp();
            RunC94Scenario(deleteProjectAfterTest);
        }

        private static void RunC94Scenario(bool deleteProjectAfterTest)
        {
            Point physPalette = PaletteOffset(new Point(205, 348));

            ConnectToArchitecture("__AutoTest", "DoDAF");

            // Step 1
            //
            CanvasInteractions.DragAndDropFromPaletteToDiagram(PaletteDiv, DiagramDiv, physPalette, new Point(359, 184));
            CanvasInteractions.DragAndDropFromPaletteToDiagram(PaletteDiv, DiagramDiv, physPalette, new Point(796, 184));
            CanvasInteractions.DragAndDropFromPaletteToDiagram(PaletteDiv, DiagramDiv, physPalette, new Point(591, 455));

            // Step 5
            //
            CanvasInteractions.ClickAndDragOnCanvas(DiagramDiv, new Point(434, 172), new Point(702, 195));
            CanvasInteractions.ClickOnCanvas(DiagramDiv, new Point(678, 180));

            // Step 6
            //
            CanvasInteractions.ClickAndDragOnCanvas(DiagramDiv, new Point(710, 172), new Point(566, 464));
            CanvasInteractions.ClickOnCanvas(DiagramDiv, new Point(532, 449));

            // Step 7
            //
            CanvasInteractions.ClickAndDragOnCanvas(DiagramDiv, new Point(537, 442), new Point(301, 193));
            CanvasInteractions.ClickOnCanvas(DiagramDiv, new Point(267, 178));

            // Step 5
            //
            SaveAndCheckInToRepository("DIV-3");

            Thread.Sleep(60000);

            if (deleteProjectAfterTest)
                DeleteDiagram("DIV-3");

        }

        private static Point PaletteOffset(Point screenCoord)
        {
            Point palleteUpperLeft = new Point(6, 317);
            return new Point(screenCoord.X - palleteUpperLeft.X, screenCoord.Y - palleteUpperLeft.Y);
        }
    }
}
