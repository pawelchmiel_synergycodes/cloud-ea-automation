﻿using Automated_Framework_2._0.Pages;
using Automated_Framework_2._0.Utility;
using Automated_Framework_2._0.Utility.Enums;
using OpenQA.Selenium;
using System;
using System.Threading;

namespace Automated_Framework_2._0.Cloud_EA.DoDAF.RepositoryManagement
{
    public class WorkspaceMgmnt : RepositoryBase
    {
        public static void CreateWorkspace(string workspaceName)
        {
            CloudEaPage.TypeInto(By.Id("NameC_I"), workspaceName);
            CloudEaPage.TypeInto(By.Id("OrgCommunityComboBoxCreateForm_I"), "JC");
            CloudEaPage.Click(By.Id("CreateBtn_CD"));
            CloudEaPage.WaitForSiteLoader();
            Logout();
        }

        public static void DeleteWorkspace(string workspaceName)
        {
            CloudEaPage.Click(By.LinkText("Delete"));
            CloudEaPage.WaitForSiteLoader();
            CloudEaPage.Click(By.CssSelector("a.primary-btn"));
            Logout();
        }

    }
}
