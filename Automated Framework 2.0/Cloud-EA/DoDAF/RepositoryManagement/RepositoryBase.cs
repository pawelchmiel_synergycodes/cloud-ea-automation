﻿using Automated_Framework_2._0.Pages;
using OpenQA.Selenium;
using System.Threading;

namespace Automated_Framework_2._0.Cloud_EA.DoDAF.RepositoryManagement
{
    public class RepositoryBase
    {
        protected static void Logout()
        {
            Thread.Sleep(1000);
            CloudEaPage.Click(By.CssSelector("i.fa.fa-sign-out"));
        }
    }
}
