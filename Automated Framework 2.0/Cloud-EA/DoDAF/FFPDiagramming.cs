﻿using System.Drawing;
using Automated_Framework_2._0.Cloud_EA.Cloud_EA_PageComponents;
using Automated_Framework_2._0.Pages;
using OpenQA.Selenium;

namespace Automated_Framework_2._0.Cloud_EA.DoDAF
{
    public class FFPDiagramming : CloudEaPage
    {
        private static readonly By PaletteDiv = By.Id("allobjectsNavBar_GC0");
        private static readonly By DiagramDiv = By.Id("myDiagram");
        public static void DropTwoObjectOntoCanvaAndMakeRelationship()
        {
            CanvasInteractions.DragAndDropFromPaletteToDiagram(PaletteDiv, DiagramDiv, new Point(100, 40), new Point(100, 100));
            CanvasInteractions.DragAndDropFromPaletteToDiagram(PaletteDiv, DiagramDiv, new Point(200, 40), new Point(400, 400));
            CanvasInteractions.DragAndDropOnCanvas(DiagramDiv, new Point(130, 100), new Point(400, 400));
        }
    }
}
