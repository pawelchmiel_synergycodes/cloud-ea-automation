﻿using Automated_Framework_2._0.Cloud_EA.Cloud_EA_PageComponents;
using Automated_Framework_2._0.Cloud_EA.Cloud_EA_PageComponents.Canvas.GraphObjects;
using Automated_Framework_2._0.Pages;
using OpenQA.Selenium;
using System.Drawing;
using System.Threading;

namespace Automated_Framework_2._0.Cloud_EA.Pages
{
    public class Sv4aPage : CloudEaPage
    {
        private static readonly By PaletteDiv = By.Id("myPalette_DODAFViewpoints");
        private static readonly By DiagramDiv = By.Id("myDiagram");
        private static readonly By ZoomToFitBtn = By.Id("ZoomToFitBtn");


        public static void CreateSimpleDiagram()
        {
            CanvasInteractions.DragAndDropFromPaletteToDiagramLeftUpperCorner<SystemFunctionSv4a>(PaletteDiv, DiagramDiv);
            CanvasInteractions.DragAndDropFromPaletteToDiagram<SystemFunctionSv4a>(PaletteDiv, DiagramDiv, new Point(680, 120));
            CanvasInteractions.DragAndDropFromPaletteToDiagram<SystemFunctionSv4a>(PaletteDiv, DiagramDiv, new Point(680, 120));
        }

        public static void ZoomToFitDiagram()
        {
            //Click(ZoomToFitBtn);
            var js = GetDriver() as IJavaScriptExecutor;
            js.ExecuteScript("document.getElementById(\"ZoomToFitBtn\").click()");
        }

        public static void ConnectToArchitecture(string workspaceName, string architectureName)
        {
            RepositoryWindowComponent.ConnectToArchitecture(workspaceName, architectureName);
        }
    }
}
