﻿using System.Drawing;
using System.Windows.Forms;
using Automated_Framework_2._0.Cloud_EA.Cloud_EA_PageComponents;
using Automated_Framework_2._0.Cloud_EA.Cloud_EA_PageComponents.Canvas.GraphObjects;
using Automated_Framework_2._0.Pages;
using OpenQA.Selenium;
using Automated_Framework_2._0.Cloud_EA.Pages;

namespace Automated_Framework_2._0.Cloud_EA.DoDAF
{
    public class Ov5aPage : DiagramAppPage
    {
        private static readonly By PaletteDiv = By.Id("myPalette_DODAFViewpoints");
        private static readonly By DiagramDiv = By.Id("myDiagram");

        public static void CreateSimpleDiagram()
        {
            // root
            CanvasInteractions.DragAndDropFromPaletteToDiagramLeftUpperCorner<OperationalActivityOv5a>(PaletteDiv, DiagramDiv);
            //root's 2 children
            CanvasInteractions.DragAndDropFromPaletteToDiagram<OperationalActivityOv5a>(PaletteDiv, DiagramDiv, new Point(680, 120));
            CanvasInteractions.DragAndDropFromPaletteToDiagram<OperationalActivityOv5a>(PaletteDiv, DiagramDiv, new Point(680, 120));
            //child of root 2 children
            CanvasInteractions.DragAndDropFromPaletteToDiagram<OperationalActivityOv5a>(PaletteDiv, DiagramDiv, new Point(600, 280));
            CanvasInteractions.DragAndDropFromPaletteToDiagram<OperationalActivityOv5a>(PaletteDiv, DiagramDiv, new Point(600, 280));
            //child of root 2 children
            CanvasInteractions.DragAndDropFromPaletteToDiagram<OperationalActivityOv5a>(PaletteDiv, DiagramDiv, new Point(760, 280));
            CanvasInteractions.DragAndDropFromPaletteToDiagram<OperationalActivityOv5a>(PaletteDiv, DiagramDiv, new Point(760, 280));
        }

        public static void RenameNodes()
        {
            CanvasInteractions.RenameNode("OA1", DiagramDiv, new Point(640, 75));
            CanvasInteractions.RenameNode("OA1.1", DiagramDiv, new Point(560, 260));
            CanvasInteractions.RenameNode("OA1.2", DiagramDiv, new Point(710, 260));
            CanvasInteractions.RenameNode("OA1.1.1", DiagramDiv, new Point(650, 440));
            CanvasInteractions.RenameNode("OA1.2.1", DiagramDiv, new Point(800, 450));
            CanvasInteractions.RenameNode("OA1.1.2", DiagramDiv, new Point(650, 620));
            CanvasInteractions.RenameNode("OA1.2.2", DiagramDiv, new Point(800, 620));
            CanvasInteractions.ClickOnCanvas(DiagramDiv, new Point(100, 100));
        }
    }
}
